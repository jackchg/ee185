# Cable Latency Report

For the fractal flyers, we will be using full-speed (12Mbps) USB over 100 foot
24 AWG Cat5e/6/7 cabling. Empirically, the important characteristic is that the
gauge of the wire is at least 24 AWG over that distance, and not whether the
cable is Cat5e/6/7.

We have determined that low-speed (1.5Mbps) USB is too much work to implement.

## Measurements

As expected, the 100 ft Cat7 cable accounts for about 150 ns of delay. With a 24
AWG cable, we have confirmed that programming works with 105 feet of cable, as
well as going through the 49-port (two-layer) USB hub.

![100-ft-cable-delay](data/100-ft-cable-delay.png)

## Frequently Asked Questions

### Why don't we use low-speed USB to increase the range?

We have determined that this would be too much work.

Currently, the programming interface to the SAM32 boards on the fractal flyers
use [CircuitPython][circuitpython], which exposes a USB mass storage device for
programming both the software as well as the firmware. However, low-speed USB
does not support the mass storage class. Specifically, the USB mass storage
class requires the use of 3 USB endpoints, two of which much be bulk transfer
endpoints (See pg 24 of [[1]]). But, USB bulk transfer is incompatible with
low-speed USB [[2]]. This is also confirmed by the tinyusb developers in [[3]].

Consequently, modifying the firmware to some one-off implementation of low-speed
USB transfer is fragile and too much work. Instead, we limit the maximum length
of cable to be within the limits of full-speed USB.

### How can I see whether a USB device connects correctly in Linux?

We can use `dmesg` to monitor the USB devices connected and disconnected. For
example, to view whether a device connects and high/full/low speed.

```
$ dmesg -L -w
```

### How can I view USB packet traces?

Wireshark can be used to capture USB packets. Follow the instructions here

https://wiki.wireshark.org/CaptureSetup/USB

## References

* [An Introduction to USB Extenders](https://www.ieci.com.au/applications/wp-usb-extender.pdf)
* [USB 2.0 Specification](http://sdphca.ucsd.edu/lab_equip_manuals/usb_20.pdf)

[1]: https://usb.org/sites/default/files/usb_msc_cbi_1.1.pdf
[2]: http://www.jungo.com/st/support/documentation/windriver/10.2.1/wdusb_manual.mhtml/USB_data_transfer_types.html
[3]: https://github.com/hathach/tinyusb/issues/284
[circuitpython]: https://www.adafruit.com/circuitpython
