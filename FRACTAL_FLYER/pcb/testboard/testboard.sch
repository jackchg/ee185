EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "FLIGHT Testing Board, EE125"
Date "2020-11-28"
Rev "1.0"
Comp "Stanford University"
Comment1 "Used to test LED strips when building them."
Comment2 "Schematic for FLIGHT LED testing board."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J2
U 1 1 5E610559
P 3800 3200
F 0 "J2" V 3650 3100 50  0000 C CNN
F 1 "LED RIGHT" V 4150 3100 50  0000 C CNN
F 2 "jst:JST_1x3" H 4000 3400 60  0001 L CNN
F 3 "" H 4000 3500 60  0001 L CNN
F 4 "CONN HEADER VERT 3POS 2.5MM" H 4000 4200 60  0001 L CNN "Description"
F 5 "JST Sales America Inc." H 4000 4300 60  0001 L CNN "Manufacturer"
F 6 "455-2248-ND" H 3800 3200 50  0001 C CNN "Digikey PN"
F 7 "B3B-XH-A(LF)(SN)" H 3800 3200 50  0001 C CNN "Manufacturer PN"
	1    3800 3200
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR09
U 1 1 5E62732A
P 4050 3450
F 0 "#PWR09" H 4050 3200 50  0001 C CNN
F 1 "GND" H 4055 3277 50  0000 C CNN
F 2 "" H 4050 3450 50  0001 C CNN
F 3 "" H 4050 3450 50  0001 C CNN
	1    4050 3450
	1    0    0    -1  
$EndComp
Text Notes 6800 3100 0    79   ~ 0
THRU-HOLE HEADERS\n    (SAMD51 I/O)
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 5E42AD98
P 6100 4800
AR Path="/5E41758F/5E42AD98" Ref="#PWR?"  Part="1" 
AR Path="/5E42AD98" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 6100 4550 50  0001 C CNN
F 1 "GND" V 6105 4672 50  0000 R CNN
F 2 "" H 6100 4800 50  0001 C CNN
F 3 "" H 6100 4800 50  0001 C CNN
	1    6100 4800
	0    1    1    0   
$EndComp
Wire Wire Line
	4000 3200 3900 3200
Wire Wire Line
	4050 3450 4050 3400
Wire Wire Line
	4050 3400 3900 3400
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J3
U 1 1 5E4888FB
P 3800 4100
F 0 "J3" V 3650 4000 50  0000 C CNN
F 1 "BODY" V 4150 4000 50  0000 C CNN
F 2 "jst:JST_1x3" H 4000 4300 60  0001 L CNN
F 3 "" H 4000 4400 60  0001 L CNN
F 4 "" H 4000 4500 60  0001 L CNN "Digi-Key_PN"
F 5 "" H 4000 4600 60  0001 L CNN "MPN"
F 6 "CONN HEADER VERT 3POS 2.5MM" H 4000 5100 60  0001 L CNN "Description"
F 7 "JST Sales America Inc." H 4000 5200 60  0001 L CNN "Manufacturer"
F 8 "455-2248-ND" H 3800 4100 50  0001 C CNN "Digikey PN"
F 9 "B3B-XH-A(LF)(SN)" H 3800 4100 50  0001 C CNN "Manufacturer PN"
	1    3800 4100
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR010
U 1 1 5E48890E
P 4050 4350
F 0 "#PWR010" H 4050 4100 50  0001 C CNN
F 1 "GND" H 4055 4177 50  0000 C CNN
F 2 "" H 4050 4350 50  0001 C CNN
F 3 "" H 4050 4350 50  0001 C CNN
	1    4050 4350
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4100 3900 4100
Wire Wire Line
	4050 4350 4050 4300
Wire Wire Line
	4050 4300 3900 4300
$Comp
L tail-rescue:640456-3-dk_Rectangular-Connectors-Headers-Male-Pins J4
U 1 1 5E48C02F
P 3800 4950
F 0 "J4" V 3650 4850 50  0000 C CNN
F 1 "LED LEFT" V 4150 4850 50  0000 C CNN
F 2 "jst:JST_1x3" H 4000 5150 60  0001 L CNN
F 3 "" H 4000 5250 60  0001 L CNN
F 4 "" H 4000 5350 60  0001 L CNN "Digi-Key_PN"
F 5 "" H 4000 5450 60  0001 L CNN "MPN"
F 6 "CONN HEADER VERT 3POS 2.5MM" H 4000 5950 60  0001 L CNN "Description"
F 7 "JST Sales America Inc." H 4000 6050 60  0001 L CNN "Manufacturer"
F 8 "455-2248-ND" H 3800 4950 50  0001 C CNN "Digikey PN"
F 9 "B3B-XH-A(LF)(SN)" H 3800 4950 50  0001 C CNN "Manufacturer PN"
	1    3800 4950
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:GND-power #PWR011
U 1 1 5E48C042
P 4050 5200
F 0 "#PWR011" H 4050 4950 50  0001 C CNN
F 1 "GND" H 4055 5027 50  0000 C CNN
F 2 "" H 4050 5200 50  0001 C CNN
F 3 "" H 4050 5200 50  0001 C CNN
	1    4050 5200
	1    0    0    -1  
$EndComp
Wire Wire Line
	4000 4950 3900 4950
Wire Wire Line
	4050 5200 4050 5150
Wire Wire Line
	4050 5150 3900 5150
Text GLabel 4000 3100 2    50   Input ~ 0
+5V_LED_1
Text GLabel 4000 4000 2    50   Input ~ 0
+5V_LED_1
Text GLabel 4000 4850 2    50   Input ~ 0
+5V_LED_1
Wire Wire Line
	6700 4800 6350 4800
NoConn ~ 6700 4600
NoConn ~ 6700 4700
NoConn ~ 6700 4900
NoConn ~ 8250 3600
NoConn ~ 8250 3700
NoConn ~ 8250 4100
NoConn ~ 8250 4200
NoConn ~ 8250 4300
NoConn ~ 8250 4400
NoConn ~ 8250 4500
NoConn ~ 8250 4600
Wire Wire Line
	3900 3300 4450 3300
Wire Wire Line
	3900 4200 4450 4200
Wire Wire Line
	3900 5050 4450 5050
Wire Wire Line
	4000 4950 4000 4850
Wire Wire Line
	4000 4000 4000 4100
Wire Wire Line
	4000 3100 4000 3200
Text Label 4450 3300 2    50   ~ 0
LED1_SIG
Text Label 4450 4200 2    50   ~ 0
LED2_SIG
Text Label 4450 5050 2    50   ~ 0
LED3_SIG
Text Label 8300 4100 0    50   ~ 0
LED1_SIG
Wire Wire Line
	8250 4100 8700 4100
Wire Wire Line
	8250 4200 8700 4200
Wire Wire Line
	8250 4300 8700 4300
Text Label 8300 4200 0    50   ~ 0
LED2_SIG
Text Label 8300 4300 0    50   ~ 0
LED3_SIG
$Comp
L tail-rescue:Conn_01x17_feather_left-Feather_connectors J?
U 1 1 5E42AD55
P 6900 4100
AR Path="/5E41758F/5E42AD55" Ref="J?"  Part="1" 
AR Path="/5E42AD55" Ref="J8"  Part="1" 
F 0 "J8" H 6950 5000 50  0000 L CNN
F 1 "Conn_01x17" H 7050 3200 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x17_P2.54mm_Vertical" H 6900 4100 50  0001 C CNN
F 3 "~" H 6900 4100 50  0001 C CNN
F 4 "DNI" H 6850 5150 50  0001 C CNN "DNI"
F 5 "40-0518-10" H 6900 4100 50  0001 C CNN "Manufacturer PN"
F 6 "Aries Electronics" H 6900 4100 50  0001 C CNN "Manufacturer"
F 7 "A460-ND" H 6900 4100 50  0001 C CNN "Digikey PN"
F 8 "" H 6900 4100 50  0001 C CNN "Descrition"
F 9 "CONN SOCKET SIP 40POS GOLD" H 6900 4100 50  0001 C CNN "Description"
	1    6900 4100
	1    0    0    -1  
$EndComp
$Comp
L Banana:24.243.1 J0
U 1 1 5FCB4F72
P 3750 1700
F 0 "J0" H 4378 1696 50  0000 L CNN
F 1 "BANANA 5V" H 4378 1605 50  0000 L CNN
F 2 "242431" H 4400 1800 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/2963540.pdf?_ga=2.199777254.991449896.1595857287-1495830251.1592303065&_gac=1.182773010.1595941966.EAIaIQobChMIoa-QjoPw6gIVEeJ3Ch0ugwDCEAAYASAAEgKbEvD_BwE" H 4400 1700 50  0001 L CNN
F 4 "PCB, SOCKET, 4MM, RED; Gender:Socket; Connector Mounting:PCB Mount; Current Rating:24A; Voltage Rating:60VDC; Contact Plating:Silver Plated Contacts; Connector Colour:Red; Contact Material:Brass; Contact Termination Type:Through HoleRoHS Compliant: Yes" H 4400 1600 50  0001 L CNN "Description"
F 5 "12.6" H 4400 1500 50  0001 L CNN "Height"
F 6 "MULTICOMP" H 4400 1400 50  0001 L CNN "Manufacturer_Name"
F 7 "24.243.1" H 4400 1300 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 4400 1200 50  0001 L CNN "Arrow Part Number"
F 9 "" H 4400 1100 50  0001 L CNN "Arrow Price/Stock"
F 10 "" H 4400 1000 50  0001 L CNN "Mouser Part Number"
F 11 "" H 4400 900 50  0001 L CNN "Mouser Price/Stock"
	1    3750 1700
	1    0    0    -1  
$EndComp
$Comp
L Banana:24.243.1 J1
U 1 1 5FCB7496
P 3750 2250
F 0 "J1" H 4378 2246 50  0000 L CNN
F 1 "BANANA GND" H 4378 2155 50  0000 L CNN
F 2 "242431" H 4400 2350 50  0001 L CNN
F 3 "http://www.farnell.com/datasheets/2963540.pdf?_ga=2.199777254.991449896.1595857287-1495830251.1592303065&_gac=1.182773010.1595941966.EAIaIQobChMIoa-QjoPw6gIVEeJ3Ch0ugwDCEAAYASAAEgKbEvD_BwE" H 4400 2250 50  0001 L CNN
F 4 "PCB, SOCKET, 4MM, RED; Gender:Socket; Connector Mounting:PCB Mount; Current Rating:24A; Voltage Rating:60VDC; Contact Plating:Silver Plated Contacts; Connector Colour:Red; Contact Material:Brass; Contact Termination Type:Through HoleRoHS Compliant: Yes" H 4400 2150 50  0001 L CNN "Description"
F 5 "12.6" H 4400 2050 50  0001 L CNN "Height"
F 6 "MULTICOMP" H 4400 1950 50  0001 L CNN "Manufacturer_Name"
F 7 "24.243.1" H 4400 1850 50  0001 L CNN "Manufacturer_Part_Number"
F 8 "" H 4400 1750 50  0001 L CNN "Arrow Part Number"
F 9 "" H 4400 1650 50  0001 L CNN "Arrow Price/Stock"
F 10 "" H 4400 1550 50  0001 L CNN "Mouser Part Number"
F 11 "" H 4400 1450 50  0001 L CNN "Mouser Price/Stock"
	1    3750 2250
	1    0    0    -1  
$EndComp
Text GLabel 3600 1600 0    50   Input ~ 0
+5V_LED_1
Wire Wire Line
	3750 1700 3600 1700
Wire Wire Line
	3600 1700 3600 1600
Wire Wire Line
	3750 1800 3600 1800
Wire Wire Line
	3600 1800 3600 1700
Connection ~ 3600 1700
$Comp
L power:GND #PWR0101
U 1 1 5FCBDABE
P 3600 2450
F 0 "#PWR0101" H 3600 2200 50  0001 C CNN
F 1 "GND" H 3605 2277 50  0000 C CNN
F 2 "" H 3600 2450 50  0001 C CNN
F 3 "" H 3600 2450 50  0001 C CNN
	1    3600 2450
	1    0    0    -1  
$EndComp
Wire Wire Line
	3750 2250 3600 2250
Wire Wire Line
	3600 2250 3600 2350
Wire Wire Line
	3750 2350 3600 2350
Connection ~ 3600 2350
Wire Wire Line
	3600 2350 3600 2450
NoConn ~ 6700 4500
NoConn ~ 6700 4400
NoConn ~ 6700 4300
NoConn ~ 6700 4200
NoConn ~ 6700 4100
NoConn ~ 6700 4000
NoConn ~ 6700 3900
NoConn ~ 6700 3800
NoConn ~ 6700 3700
NoConn ~ 6700 3500
NoConn ~ 6700 3400
NoConn ~ 6700 3300
Wire Wire Line
	6700 3600 6350 3600
Connection ~ 6350 4800
Wire Wire Line
	6350 4800 6100 4800
Wire Wire Line
	6350 3600 6350 4800
Wire Wire Line
	8150 1050 8000 1050
Wire Wire Line
	8000 1050 8000 1250
Connection ~ 8000 1250
Wire Wire Line
	8150 1250 8000 1250
Wire Wire Line
	8000 1250 8000 1450
Connection ~ 8000 1450
Wire Wire Line
	8150 1450 8000 1450
Wire Wire Line
	8000 1450 8000 1650
Wire Wire Line
	8000 1650 8000 1750
Connection ~ 8000 1650
Wire Wire Line
	8150 1650 8000 1650
$Comp
L tail-rescue:GND-power #PWR0102
U 1 1 5E58E9EF
P 8000 1750
F 0 "#PWR0102" H 8000 1500 50  0001 C CNN
F 1 "GND" H 8005 1577 50  0000 C CNN
F 2 "" H 8000 1750 50  0001 C CNN
F 3 "" H 8000 1750 50  0001 C CNN
	1    8000 1750
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH4
U 1 1 5E586ED6
P 8150 1650
F 0 "MH4" V 8150 1838 50  0000 L CNN
F 1 "Mounting Hole" V 8195 1838 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8350 1650 50  0001 C CNN
F 3 "~" H 8350 1650 50  0001 C CNN
	1    8150 1650
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH3
U 1 1 5E586AF5
P 8150 1450
F 0 "MH3" V 8150 1638 50  0000 L CNN
F 1 "Mounting Hole" V 8195 1638 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8350 1450 50  0001 C CNN
F 3 "~" H 8350 1450 50  0001 C CNN
	1    8150 1450
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH2
U 1 1 5E5865A5
P 8150 1250
F 0 "MH2" V 8150 1438 50  0000 L CNN
F 1 "Mounting Hole" V 8195 1438 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8350 1250 50  0001 C CNN
F 3 "~" H 8350 1250 50  0001 C CNN
	1    8150 1250
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH1
U 1 1 5E584D96
P 8150 1050
F 0 "MH1" V 8150 1238 50  0000 L CNN
F 1 "Mounting Hole" V 8195 1238 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8350 1050 50  0001 C CNN
F 3 "~" H 8350 1050 50  0001 C CNN
	1    8150 1050
	0    1    1    0   
$EndComp
NoConn ~ 8250 3800
NoConn ~ 8250 3900
NoConn ~ 8250 4000
$Comp
L tail-rescue:Conn_01x14_feather_right-Feather_connectors J?
U 1 1 5E42AD5B
P 8050 4200
AR Path="/5E41758F/5E42AD5B" Ref="J?"  Part="1" 
AR Path="/5E42AD5B" Ref="J9"  Part="1" 
F 0 "J9" H 8500 4900 50  0000 C CNN
F 1 "Conn_01x14" H 8150 3400 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 8050 4200 50  0001 C CNN
F 3 "~" H 8050 4200 50  0001 C CNN
F 4 "40-0518-10" H 8050 4200 50  0001 C CNN "Manufacturer PN"
F 5 "Aries Electronics" H 8050 4200 50  0001 C CNN "Manufacturer"
F 6 "A460-ND" H 8050 4200 50  0001 C CNN "Digikey PN"
F 7 "CONN SOCKET SIP 40POS GOLD" H 8050 4200 50  0001 C CNN "Description"
	1    8050 4200
	-1   0    0    -1  
$EndComp
$EndSCHEMATC
