EESchema Schematic File Version 4
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title "FLIGHT Testing Board, EE125"
Date "2020-11-28"
Rev "1.0"
Comp "Stanford University"
Comment1 "Used to test LED strips when building them."
Comment2 "Schematic for FLIGHT LED testing board."
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 5E42AD98
P 1000 2600
AR Path="/5E41758F/5E42AD98" Ref="#PWR?"  Part="1" 
AR Path="/5E42AD98" Ref="#PWR020"  Part="1" 
F 0 "#PWR020" H 1000 2350 50  0001 C CNN
F 1 "GND" V 1005 2472 50  0000 R CNN
F 2 "" H 1000 2600 50  0001 C CNN
F 3 "" H 1000 2600 50  0001 C CNN
	1    1000 2600
	0    1    1    0   
$EndComp
Text Label 3200 1900 0    50   ~ 0
LED1_SIG
Text Label 3200 2000 0    50   ~ 0
LED2_SIG
Text Label 3200 2100 0    50   ~ 0
LED3_SIG
Wire Wire Line
	8550 1050 8400 1050
Wire Wire Line
	8400 1050 8400 1250
Connection ~ 8400 1250
Wire Wire Line
	8550 1250 8400 1250
Wire Wire Line
	8400 1250 8400 1450
Wire Wire Line
	8400 1450 8400 1550
Connection ~ 8400 1450
Wire Wire Line
	8550 1450 8400 1450
$Comp
L tail-rescue:GND-power #PWR0102
U 1 1 5E58E9EF
P 8400 1550
F 0 "#PWR0102" H 8400 1300 50  0001 C CNN
F 1 "GND" H 8405 1377 50  0000 C CNN
F 2 "" H 8400 1550 50  0001 C CNN
F 3 "" H 8400 1550 50  0001 C CNN
	1    8400 1550
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH4
U 1 1 5E586ED6
P 8550 1450
F 0 "MH4" V 8550 1638 50  0000 L CNN
F 1 "Mounting Hole" V 8595 1638 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8750 1450 50  0001 C CNN
F 3 "~" H 8750 1450 50  0001 C CNN
	1    8550 1450
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH3
U 1 1 5E586AF5
P 8550 1250
F 0 "MH3" V 8550 1438 50  0000 L CNN
F 1 "Mounting Hole" V 8595 1438 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8750 1250 50  0001 C CNN
F 3 "~" H 8750 1250 50  0001 C CNN
	1    8550 1250
	0    1    1    0   
$EndComp
$Comp
L tail-rescue:TestPoint-Connector MH2
U 1 1 5E5865A5
P 8550 1050
F 0 "MH2" V 8550 1238 50  0000 L CNN
F 1 "Mounting Hole" V 8595 1238 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8750 1050 50  0001 C CNN
F 3 "~" H 8750 1050 50  0001 C CNN
	1    8550 1050
	0    1    1    0   
$EndComp
Connection ~ 8400 1050
$Comp
L tail-rescue:TestPoint-Connector MH1
U 1 1 5E584D96
P 8550 850
F 0 "MH1" V 8550 1038 50  0000 L CNN
F 1 "Mounting Hole" V 8595 1038 50  0001 L CNN
F 2 "MountingHole:MountingHole_3.2mm_M3_Pad" H 8750 850 50  0001 C CNN
F 3 "~" H 8750 850 50  0001 C CNN
	1    8550 850 
	0    1    1    0   
$EndComp
Wire Wire Line
	8400 850  8400 1050
Wire Wire Line
	8550 850  8400 850 
Text Notes 1700 900  0    79   ~ 0
UPPER HEADERS\n(SAMD51 I/O)
$Comp
L tail-rescue:Conn_01x17_feather_left-Feather_connectors J?
U 1 1 5E42AD55
P 1800 1900
AR Path="/5E41758F/5E42AD55" Ref="J?"  Part="1" 
AR Path="/5E42AD55" Ref="J8"  Part="1" 
F 0 "J8" H 1850 2800 50  0000 L CNN
F 1 "Conn_01x17" H 1950 1000 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x17_P2.54mm_Vertical" H 1800 1900 50  0001 C CNN
F 3 "~" H 1800 1900 50  0001 C CNN
F 4 "DNI" H 1750 2950 50  0001 C CNN "DNI"
F 5 "40-0518-10" H 1800 1900 50  0001 C CNN "Manufacturer PN"
F 6 "Aries Electronics" H 1800 1900 50  0001 C CNN "Manufacturer"
F 7 "A460-ND" H 1800 1900 50  0001 C CNN "Digikey PN"
F 8 "" H 1800 1900 50  0001 C CNN "Descrition"
F 9 "CONN SOCKET SIP 40POS GOLD" H 1800 1900 50  0001 C CNN "Description"
	1    1800 1900
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:Conn_01x14_feather_right-Feather_connectors J?
U 1 1 5E42AD5B
P 2950 2000
AR Path="/5E41758F/5E42AD5B" Ref="J?"  Part="1" 
AR Path="/5E42AD5B" Ref="J9"  Part="1" 
F 0 "J9" H 3400 2700 50  0000 C CNN
F 1 "Conn_01x14" H 3050 1200 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 2950 2000 50  0001 C CNN
F 3 "~" H 2950 2000 50  0001 C CNN
F 4 "40-0518-10" H 2950 2000 50  0001 C CNN "Manufacturer PN"
F 5 "Aries Electronics" H 2950 2000 50  0001 C CNN "Manufacturer"
F 6 "A460-ND" H 2950 2000 50  0001 C CNN "Digikey PN"
F 7 "CONN SOCKET SIP 40POS GOLD" H 2950 2000 50  0001 C CNN "Description"
	1    2950 2000
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 1600 1350 1600
Wire Wire Line
	1600 1700 1350 1700
Wire Wire Line
	1600 1800 1350 1800
Wire Wire Line
	1600 1900 1350 1900
Wire Wire Line
	1600 2000 1350 2000
Wire Wire Line
	1600 2100 1350 2100
Wire Wire Line
	1600 2200 1350 2200
Wire Wire Line
	1600 2300 1350 2300
Wire Wire Line
	1600 2400 1350 2400
Wire Wire Line
	1600 2500 1350 2500
Wire Wire Line
	1600 2700 1350 2700
Connection ~ 1250 2600
Wire Wire Line
	1250 2600 1000 2600
Wire Wire Line
	1600 2600 1250 2600
Wire Wire Line
	1600 1200 1350 1200
Wire Wire Line
	1600 1500 1350 1500
Wire Wire Line
	1600 1300 1350 1300
Wire Wire Line
	1250 1400 1250 2600
Wire Wire Line
	1600 1400 1250 1400
Wire Wire Line
	1600 1100 1350 1100
Text Label 1350 1300 0    50   ~ 0
AREF
Text Label 1350 1200 0    50   ~ 0
3V3
Text Label 1350 1100 0    50   ~ 0
RST
Text Label 1350 1500 0    50   ~ 0
A0
Text Label 1350 1600 0    50   ~ 0
A1
Text Label 1350 1700 0    50   ~ 0
A2
Text Label 1350 1800 0    50   ~ 0
A3
Text Label 1350 1900 0    50   ~ 0
A4
Text Label 1350 2000 0    50   ~ 0
A5
Text Label 1350 2100 0    50   ~ 0
SCK
Text Label 1350 2200 0    50   ~ 0
MO
Text Label 1350 2300 0    50   ~ 0
MI
Text Label 1350 2400 0    50   ~ 0
RX
Wire Wire Line
	3150 1400 3600 1400
Wire Wire Line
	3150 1500 3600 1500
Wire Wire Line
	3150 1600 3600 1600
Wire Wire Line
	3150 1700 3600 1700
Wire Wire Line
	3150 1800 3600 1800
Wire Wire Line
	3150 2700 3600 2700
Wire Wire Line
	3150 2600 3600 2600
Wire Wire Line
	3150 2500 3600 2500
Wire Wire Line
	3150 2400 3600 2400
Wire Wire Line
	3150 2300 3600 2300
Wire Wire Line
	3150 2200 3600 2200
Wire Wire Line
	3150 2100 3600 2100
Wire Wire Line
	3150 2000 3600 2000
Wire Wire Line
	3150 1900 3600 1900
Text Label 3600 1400 0    50   ~ 0
BAT
Text Label 3600 1500 0    50   ~ 0
BATT
Text Label 3600 1600 0    50   ~ 0
En
Text Label 3600 1700 0    50   ~ 0
USB
Text Label 3600 1800 0    50   ~ 0
13
Text Label 3600 1900 0    50   ~ 0
12
Text Label 3600 2000 0    50   ~ 0
11
Text Label 3600 2100 0    50   ~ 0
10
Text Label 3600 2200 0    50   ~ 0
9
Text Label 3600 2300 0    50   ~ 0
6
Text Label 3600 2400 0    50   ~ 0
5
Text Label 3600 2500 0    50   ~ 0
SCL
Text Label 3600 2600 0    50   ~ 0
SDA
Text Label 3600 2700 0    50   ~ 0
AIN9
Text Label 1350 2700 0    50   ~ 0
D16
Text Label 1350 2500 0    50   ~ 0
TX
$Comp
L tail-rescue:GND-power #PWR?
U 1 1 5FEB694C
P 1000 4950
AR Path="/5E41758F/5FEB694C" Ref="#PWR?"  Part="1" 
AR Path="/5FEB694C" Ref="#PWR0101"  Part="1" 
F 0 "#PWR0101" H 1000 4700 50  0001 C CNN
F 1 "GND" V 1005 4822 50  0000 R CNN
F 2 "" H 1000 4950 50  0001 C CNN
F 3 "" H 1000 4950 50  0001 C CNN
	1    1000 4950
	0    1    1    0   
$EndComp
Text Label 3200 4250 0    50   ~ 0
LED1_SIG
Text Label 3200 4350 0    50   ~ 0
LED2_SIG
Text Label 3200 4450 0    50   ~ 0
LED3_SIG
Text Notes 1700 3250 0    79   ~ 0
LOWER HEADERS\n(FLIGHT TAILBOARD)
$Comp
L tail-rescue:Conn_01x17_feather_left-Feather_connectors J?
U 1 1 5FEB695C
P 1800 4250
AR Path="/5E41758F/5FEB695C" Ref="J?"  Part="1" 
AR Path="/5FEB695C" Ref="J1"  Part="1" 
F 0 "J1" H 1850 5150 50  0000 L CNN
F 1 "Conn_01x17" H 1950 3350 50  0000 L CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x17_P2.54mm_Vertical" H 1800 4250 50  0001 C CNN
F 3 "~" H 1800 4250 50  0001 C CNN
F 4 "DNI" H 1750 5300 50  0001 C CNN "DNI"
F 5 "40-0518-10" H 1800 4250 50  0001 C CNN "Manufacturer PN"
F 6 "Aries Electronics" H 1800 4250 50  0001 C CNN "Manufacturer"
F 7 "A460-ND" H 1800 4250 50  0001 C CNN "Digikey PN"
F 8 "" H 1800 4250 50  0001 C CNN "Descrition"
F 9 "CONN SOCKET SIP 40POS GOLD" H 1800 4250 50  0001 C CNN "Description"
	1    1800 4250
	1    0    0    -1  
$EndComp
$Comp
L tail-rescue:Conn_01x14_feather_right-Feather_connectors J?
U 1 1 5FEB6966
P 2950 4350
AR Path="/5E41758F/5FEB6966" Ref="J?"  Part="1" 
AR Path="/5FEB6966" Ref="J2"  Part="1" 
F 0 "J2" H 3400 5050 50  0000 C CNN
F 1 "Conn_01x14" H 3050 3550 50  0000 C CNN
F 2 "Connector_PinSocket_2.54mm:PinSocket_1x14_P2.54mm_Vertical" H 2950 4350 50  0001 C CNN
F 3 "~" H 2950 4350 50  0001 C CNN
F 4 "40-0518-10" H 2950 4350 50  0001 C CNN "Manufacturer PN"
F 5 "Aries Electronics" H 2950 4350 50  0001 C CNN "Manufacturer"
F 6 "A460-ND" H 2950 4350 50  0001 C CNN "Digikey PN"
F 7 "CONN SOCKET SIP 40POS GOLD" H 2950 4350 50  0001 C CNN "Description"
	1    2950 4350
	-1   0    0    -1  
$EndComp
Wire Wire Line
	1600 3950 1350 3950
Wire Wire Line
	1600 4050 1350 4050
Wire Wire Line
	1600 4150 1350 4150
Wire Wire Line
	1600 4250 1350 4250
Wire Wire Line
	1600 4350 1350 4350
Wire Wire Line
	1600 4450 1350 4450
Wire Wire Line
	1600 4550 1350 4550
Wire Wire Line
	1600 4650 1350 4650
Wire Wire Line
	1600 4750 1350 4750
Wire Wire Line
	1600 4850 1350 4850
Wire Wire Line
	1600 5050 1350 5050
Connection ~ 1250 4950
Wire Wire Line
	1250 4950 1000 4950
Wire Wire Line
	1600 4950 1250 4950
Wire Wire Line
	1600 3550 1350 3550
Wire Wire Line
	1600 3850 1350 3850
Wire Wire Line
	1600 3650 1350 3650
Wire Wire Line
	1250 3750 1250 4950
Wire Wire Line
	1600 3750 1250 3750
Wire Wire Line
	1600 3450 1350 3450
Text Label 1350 3650 0    50   ~ 0
AREF
Text Label 1350 3550 0    50   ~ 0
3V3
Text Label 1350 3450 0    50   ~ 0
RST
Text Label 1350 3850 0    50   ~ 0
A0
Text Label 1350 3950 0    50   ~ 0
A1
Text Label 1350 4050 0    50   ~ 0
A2
Text Label 1350 4150 0    50   ~ 0
A3
Text Label 1350 4250 0    50   ~ 0
A4
Text Label 1350 4350 0    50   ~ 0
A5
Text Label 1350 4450 0    50   ~ 0
SCK
Text Label 1350 4550 0    50   ~ 0
MO
Text Label 1350 4650 0    50   ~ 0
MI
Text Label 1350 4750 0    50   ~ 0
RX
Wire Wire Line
	3150 3750 3600 3750
Wire Wire Line
	3150 3850 3600 3850
Wire Wire Line
	3150 3950 3600 3950
Wire Wire Line
	3150 4050 3600 4050
Wire Wire Line
	3150 4150 3600 4150
Wire Wire Line
	3150 5050 3600 5050
Wire Wire Line
	3150 4950 3600 4950
Wire Wire Line
	3150 4850 3600 4850
Wire Wire Line
	3150 4750 3600 4750
Wire Wire Line
	3150 4650 3600 4650
Wire Wire Line
	3150 4550 3600 4550
Wire Wire Line
	3150 4450 3600 4450
Wire Wire Line
	3150 4350 3600 4350
Wire Wire Line
	3150 4250 3600 4250
Text Label 3600 3750 0    50   ~ 0
BAT
Text Label 3600 3850 0    50   ~ 0
BATT
Text Label 3600 3950 0    50   ~ 0
En
Text Label 3600 4050 0    50   ~ 0
USB
Text Label 3600 4150 0    50   ~ 0
13
Text Label 3600 4250 0    50   ~ 0
12
Text Label 3600 4350 0    50   ~ 0
11
Text Label 3600 4450 0    50   ~ 0
10
Text Label 3600 4550 0    50   ~ 0
9
Text Label 3600 4650 0    50   ~ 0
6
Text Label 3600 4750 0    50   ~ 0
5
Text Label 3600 4850 0    50   ~ 0
SCL
Text Label 3600 4950 0    50   ~ 0
SDA
Text Label 3600 5050 0    50   ~ 0
AIN9
Text Label 1350 5050 0    50   ~ 0
D16
Text Label 1350 4850 0    50   ~ 0
TX
$EndSCHEMATC
