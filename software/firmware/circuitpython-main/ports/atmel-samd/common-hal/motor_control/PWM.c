#include "common-hal/microcontroller/Pin.h"
#include "peripherals/samd/pins.h"
#include "shared-bindings/pwmio/PWMOut.h"
#include <stdbool.h>
#include <stdio.h>

#define MOTOR_PWM_FREQUENCY 10000 //Frequency to PWM at, issues on SAM32 at 20000

/*The pins below are example pins that can be used for outputting PWM.
For winter_2021_PID_demo.py, only pin D43 on the sam32 was used. */

/*Additional pin numbers and registers can be found under:
     ports/atmel-samd/boards/feather_m4_express/pins.c
     ports/atmel-samd/boards/sam32/pins.c */

static pwmio_pwmout_obj_t left_up_wing;
static bool left_up_inited;
static const mcu_pin_obj_t *left_up_pin = &pin_PA22; // Use &pin_PB09 for A3 on feather m4. Use &pin_PA22 for D43 on sam32

static pwmio_pwmout_obj_t left_down_wing;
static bool left_down_inited;
static const mcu_pin_obj_t *left_down_pin = &pin_PA05; // Use &pin_PA05 for A1 on feather m4. Use &pin_PA05 for DAC1 on sam32

static pwmio_pwmout_obj_t right_up_wing;
static bool right_up_inited;
static const mcu_pin_obj_t *right_up_pin = &pin_PA07; // Use &pin_PB17 for RX on feather m4. Use &pin_PA07 for D16 on sam32

static pwmio_pwmout_obj_t right_down_wing;
static bool right_down_inited;
static const mcu_pin_obj_t *right_down_pin = &pin_PB16; // Use &pin_PB16 for TX on feather m4. Use &pin_PA16 for D35 on sam32 

extern pwmout_result_t common_hal_pwmio_pwmout_construct (
    pwmio_pwmout_obj_t *self, const mcu_pin_obj_t *pin, uint16_t duty,
    uint32_t frequency, bool variable_frequency);

void set_pwm (bool left, bool up, float duty);

void
set_pwm (bool left, bool up, float fduty)
{
  if (fduty > 1 || fduty < 0)
    {
      printf ("Bad duty value\n");
      return;
    }
  //Convert from the float to a fraction of 65536
  float duty_scaled = fduty * 65536; // 2 ** 16
  int duty = (int)duty_scaled;
  //Switch through the pins, initializing if needed
  if (left)
    {
      if (up)
        {
          if (!left_up_inited)
            {
              common_hal_pwmio_pwmout_construct (&left_up_wing, left_up_pin,
                                                 duty, MOTOR_PWM_FREQUENCY,
                                                 false);
              left_up_inited = true;
            }
          else
            {
              common_hal_pwmio_pwmout_set_duty_cycle (&left_up_wing, duty);
            }
        }
      else
        {
          if (!left_down_inited)
            {
              common_hal_pwmio_pwmout_construct (&left_down_wing,
                                                 left_down_pin, duty,
                                                 MOTOR_PWM_FREQUENCY, false);
              left_down_inited = true;
            }
          else
            {
              common_hal_pwmio_pwmout_set_duty_cycle (&left_down_wing, duty);
            }
        }
    }
  else
    {
      if (up)
        {
          if (!right_up_inited)
            {
              common_hal_pwmio_pwmout_construct (&right_up_wing, right_up_pin, duty,
                                                 MOTOR_PWM_FREQUENCY, false);
              right_up_inited = true;
            }
          else
            {
              common_hal_pwmio_pwmout_set_duty_cycle (&right_up_wing, duty);
            }
        }
      else
        {
          if (!right_down_inited)
            {
              common_hal_pwmio_pwmout_construct (&right_down_wing, right_down_pin, duty,
                                                 MOTOR_PWM_FREQUENCY, false);
              right_down_inited = true;
            }
          else
            {
              common_hal_pwmio_pwmout_set_duty_cycle (&right_down_wing, duty);
            }
        }
    }

  return;
}