/*
 * This file is part of the MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * SPDX-FileCopyrightText: Copyright (c) 2016 Damien P. George
 *
 * Permission is hereby granted, free of charge, to any person obtaining a copy
 * of this software and associated documentation files (the "Software"), to deal
 * in the Software without restriction, including without limitation the rights
 * to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
 * copies of the Software, and to permit persons to whom the Software is
 * furnished to do so, subject to the following conditions:
 *
 * The above copyright notice and this permission notice shall be included in
 * all copies or substantial portions of the Software.
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
 * IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
 * FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
 * AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
 * LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
 * OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
 * THE SOFTWARE.
 */


#include <stdint.h>

#include "hal/include/hal_gpio.h"

#include "mpconfigport.h"
//#include "samd/pins.h"
#include "ports/atmel-samd/peripherals/samd/pins.h"
#include "samd/timers.h"
#include "py/gc.h"
#include "py/runtime.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"
#include "supervisor/shared/translate.h"
#include "timer_handler.h"
#include "ports/atmel-samd/peripherals/samd/timers.h"
#include <stdio.h>
#include "mpconfigport.h"
#include "ports/atmel-samd/timer_handler.h"
#include "ports/atmel-samd/common-hal/motor_control/PID.h"
#include "ports/atmel-samd/common-hal/motor_control/PWM.c"

static uint8_t control_tc_index = 0xff;
//static uint8_t interval_length = 40;
//static volatile uint8_t counter = 0;
static volatile uint32_t current_compare = 0;
//static float left_wing_angle;
//static float right_wing_angle;
//static float duty_left; //Note: Only used for testing if you can get a duty cycle value from set_wing_angle. To perform this test, change set_wing_angle to a float type function
//static float duty_right; //Note: Only used for testing if you can get a duty cycle value from set_wing_angle. To perform this test, change set_wing_angle to a float type function

#define LEFT true
#define RIGHT false

//Interrupt handler function for performing PID calculation
void control_interrupt_handler(uint8_t index) {
  if (index != control_tc_index) return;

  Tc* tc = tc_insts[index];
  if (!tc->COUNT16.INTFLAG.bit.MC0) return;

  set_wing_angle(LEFT,degrees);        //For setting the LEFT wing angle (degrees is a global variable whose value originates from the Python function control.tasks(degrees))
  //set_wing_angle(RIGHT, degrees);    //For setting the RIGHT wing angle (angle needs a global variable from Python script)

  // Clear the interrupt bit.
  tc->COUNT16.INTFLAG.reg = TC_INTFLAG_MC0;
}

//Construct for initializing timer for timer interrupt 
void common_hal_motor_construct(void) {
    int refcount = 0;
    if (refcount == 0) {
        // Find a spare timer.
        Tc *tc = NULL;
        int8_t index = TC_INST_NUM - 1;
        for (; index >= 0; index--) {
            if (tc_insts[index]->COUNT16.CTRLA.bit.ENABLE == 0) {
                tc = tc_insts[index];
                break;
            }
        }
        if (tc == NULL) {
            mp_raise_RuntimeError(translate("All timers in use"));
        }

	control_tc_index = index;

        set_timer_handler(true, index, TC_HANDLER_CONTROL);
        // We use GCLK0 for SAMD21 and GCLK1 for SAMD51 because they both run at 48mhz making our
        // math the same across the boards.
        #ifdef SAMD21
        turn_on_clocks(true, index, 0);
        #endif
        #ifdef SAM_D5X_E5X
        turn_on_clocks(true, index, 1);
        #endif


        #ifdef SAMD21
        tc->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 |
                                TC_CTRLA_PRESCALER_DIV64 |
                                TC_CTRLA_WAVEGEN_NFRQ;
        #endif
        #ifdef SAM_D5X_E5X
        tc_reset(tc);
        tc_set_enable(tc, false);
        tc->COUNT16.CTRLA.reg = TC_CTRLA_MODE_COUNT16 | TC_CTRLA_PRESCALER_DIV64;
        tc->COUNT16.WAVE.reg = TC_WAVE_WAVEGEN_NFRQ;
        #endif

        tc_set_enable(tc, true);
        tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_STOP;
    
	refcount++;
    }
    }


//The function that enables the interrupts so that they fire and activate the PWM
void common_hal_motor_PID(void){
    Tc* tc = tc_insts[control_tc_index];
    tc->COUNT16.CTRLA.bit.ENABLE = 1;
    tc->COUNT8.CC[0].reg = current_compare; //I believe this sets the upper limit of the counter for the interrupt timer. Value is currently 0, but changing this value has not been explored during winter 2021.

    // Clear our interrupt in case it was set earlier
    tc->COUNT16.INTFLAG.reg = TC_INTFLAG_MC0;
    tc->COUNT16.INTENSET.reg = TC_INTENSET_MC0;
    tc_enable_interrupts(control_tc_index);

    tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_RETRIGGER; //Retriggers interrupts so that the interrupt counter will start and move towards its upper limit. 
    
    //tc->COUNT16.CTRLBSET.reg = TC_CTRLBSET_CMD_STOP;
    //tc->COUNT16.INTENCLR.reg = TC_INTENCLR_MC0;
    //printf("The duty cycle is %f", (double) duty_left);  //Used for testing. See note at the beginning
}

/*The function that updates the variable "degrees" with the new setpoint value from Python's control.tasks(degrees).
This function disables interrupts first and then updates "degrees".
The interrupts get reenabled with common_hal_motor_PID() is called. */
void control_set_degrees(float d){
    tc_disable_interrupts(control_tc_index);
    degrees = d;
}
