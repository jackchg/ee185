#include "ports/atmel-samd/common-hal/motor_control/PID.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"
#include "shared-bindings/adafruit_bus_device/SPIDevice.h"
#include "shared-bindings/busio/SPI.h"
//#include "common-hal/busio/SPI.h"
#include "peripherals/samd/pins.h"

#include "shared-bindings/digitalio/DigitalInOut.h"

#include <stdio.h>
#include <stdbool.h>
#include <stdlib.h>
#include <math.h>
#define ERR_THRESHOLD_DEGREES 2
#define BEGIN_SLOW_DEGREES 40
#define MAX_DUTY  0.5 //Set to PWM %high that represents max speed we want the wings to move
#define UP true
#define DOWN false
#define LEFT true
#define RIGHT false
//define LEFT and RIGHT
//#define board_sam32 //COMMENT THIS OUT FOR FEATHER M4
//#ifdef board_sam32 //sam32 pins
#define SCK &pin_PB13 //SCK
#define MOSI &pin_PB12 //MOSI
#define MISO &pin_PB14 //MISO
#define CS_LEFT &pin_PA14 //Pin D31 on SAM32
#define CS_CENTER &pin_PB31 //Pin D60 on SAM32
#define CS_RIGHT &pin_PB00  //Pin D61 on SAM32
//#else //feather m4 pins
//    #define SCK &pin_PA17 //SCK
//    #define MOSI &pin_PB23 //MOSI
//    #define MISO &pin_PB22 //MISO
//    #define CS_LEFT &pin_PA18 //Pin D6 on feather m4
//    #define CS_CENTER &pin_PA19 //Pin D9 on feather m4
//    #define CS_RIGHT &pin_PA16  //Pin D5 on feather m4
//#endif

// Register addresses:
#define _REG_OUTADC1_L   0x08
#define _REG_WHOAMI      0x0F
#define _REG_TEMPCFG     0x1F
#define _REG_CTRL1       0x20
#define _REG_CTRL3       0x22
#define _REG_CTRL4       0x23
#define _REG_CTRL5       0x24
#define _REG_OUT_X_L     0x28
#define _REG_OUT_X_H     0x29
#define _REG_OUT_Y_L     0x2A
#define _REG_OUT_Y_H     0x2B
#define _REG_OUT_Z_L     0x2C
#define _REG_OUT_Z_H     0x2D
#define _REG_INT1SRC     0x31
#define _REG_CLICKCFG    0x38
#define _REG_CLICKSRC    0x39
#define _REG_CLICKTHS    0x3A
#define _REG_TIMELIMIT   0x3B
#define _REG_TIMELATENCY 0x3C
#define _REG_TIMEWINDOW  0x3D

// Register value constants:
#define RANGE_16_G 0x03   // +/- 16g
#define RANGE_8_G  0x02   // +/- 8g
#define RANGE_4_G  0x01   // +/- 4g
#define RANGE_2_G  0x00   // +/- 2g (default value)
#define DATARATE_1344_HZ 0x09  // 1.344 KHz
#define DATARATE_400_HZ  0x07  // 400Hz
#define DATARATE_200_HZ  0x06  // 200Hz
#define DATARATE_100_HZ  0x05  // 100Hz
#define DATARATE_50_HZ   0x04  // 50Hz
#define DATARATE_25_HZ   0x03  // 25Hz
#define DATARATE_10_HZ   0x02  // 10 Hz
#define DATARATE_1_HZ    0x01  // 1 Hz
#define DATARATE_POWERDOWN 0
#define DATARATE_LOWPOWER_1K6HZ 0x1000
#define DATARATE_LOWPOWER_5KHZ  0x1001

// Other constants
#define STANDARD_GRAVITY  9.806

busio_spi_obj_t spi;

typedef struct spi_sensor_t {
    adafruit_bus_device_spidevice_obj_t sensor;
    uint8_t gbuf[6];
    digitalio_digitalinout_obj_t cs;
} spi_sensor_t;
/*adafruit_bus_device_spidevice_obj_t sensor_left;
uint8_t gbuf_left[6];
digitalio_digitalinout_obj_t cs;
*/
spi_sensor_t sensor_left;
spi_sensor_t sensor_center;
spi_sensor_t sensor_right;


static uint8_t _read_register_byte(spi_sensor_t *device,uint8_t reg);
extern void set_pwm(bool left, bool up, float duty);
acceleration read_accelerometer(spi_sensor_t *device);
void initSensors(spi_sensor_t *device);
void set_data_rate(spi_sensor_t *device, uint8_t rate);

/*The following is based on the EE285 control loop python code (see the repository for details)*/

//Returns the angle of the bird (top plate acceleration)
float get_bird_angle(void){
    //acceleration bird;                          // bird acceleration struc created 
    //bird = read_accelerometer(&sensor_center);  // bird takes in values from accelerometer
    float bird_x = 0.0;                           // X acceleration value (number currently hard-coded for testing with Python script) 
    //float bird_y = 0.0;                         // Y acceleration value (number currently hard-coded for testing with Python script)
    float bird_z = 9.8;                           // Z acceleration value (number currently hard-coded for testing with Python script)
    //float x_y_accel = abs(bird.x) + abs(bird.y);// Safety mechanism for stoping wing if it hits hard object. Not fully impletemented in C code. See python control loop in repository for details.

    if (bird_z <0.0002)                           // This is to make sure the tangent calculation does not go to infinity 
    {
        bird_z = 0.0005;
    }

    float tan_pitch = bird_x/bird_z;                //Tangent between X and Z accelerometer vectors for wing pitch.
    float angle_rads = atanf(tan_pitch);            //Arc tangent calculation
    float angle_degrees = angle_rads * 180 / 3.14;  //Convert from radians to degrees
    float center_angle = angle_degrees;
    return center_angle;
}

float get_wing_angle(bool left){ ///Will take in sensor values from WING accelerometer (LEFT or RIGHT depends on boolean)
    float bird_angle = get_bird_angle();
    float wing_y;
    float wing_z;
    acceleration wing;                              //variable "wing" with accceleration struct
    wing = read_accelerometer(&sensor_center);      //accelerometer value is read (for the test done in the Python script, the pinout for the body accelerometer was used)
        if (left){      //for left wing
            //float wing_x = 0.0;                   //X acceleration value from wing sensor
            wing_y = wing.y;                        //Y acceleration value from wing sensor 
            wing_z = wing.z;                        //Z acceleration value from wing sensor 
        }
        else {          //for right wing
            //wing_x = 0.0;                         //X acceleration value from wing sensor
            wing_y = wing.y;                        //Y acceleration value from wing sensor 
            wing_z = wing.z;                        //Z acceleration value from wing sensor
        }

    if (wing_z < 0.0002){                           // This is to make sure the tangent calculation does not go to infinity 
        wing_z = 0.0005;
    }
    float tan_pitch =  wing_y/wing_z;               //Tangent and angle calculations performed. Similar to the calculations done in get_bird_angle()
    float angle_rads = atanf(tan_pitch);
    float angle_degrees = angle_rads * 180 / 3.14;
    if (left){
        angle_degrees += bird_angle;
    }
    else {
        angle_degrees -= bird_angle;
    }
    float wing_angle = angle_degrees;
    return wing_angle;
}

//This function takes the degree setpoint from Python and calculates the necessary duty cycle to output. 
//Essentially the heart of the PID control loop.

void set_wing_angle(bool left, int degrees_C){ 
    float current_angle = get_wing_angle(left);
    float diff = abs(current_angle-degrees_C);
    float duty = 0;
    if (diff < ERR_THRESHOLD_DEGREES){
        duty = 0;
    }
    else if (diff < BEGIN_SLOW_DEGREES){
        duty = ((diff - ERR_THRESHOLD_DEGREES)/(BEGIN_SLOW_DEGREES-1)) * MAX_DUTY;
    }
    else {
        duty = MAX_DUTY;
    }

    if (current_angle < degrees_C){
        if(left){ //send the PWM signal to move the left wing up
            set_pwm(LEFT,UP,duty);
        }
        else{ //send the PWM signal moves the right wing up 
            set_pwm(RIGHT,UP,duty);
        }

    }

    else {
        if(left){  //send a PWM signal to stop the left wing (TODO: program this so that it moves the left wing down)
            set_pwm(LEFT, UP, 0);
        }
        else{      //send a PWM signal to stop the right wing (TODO: program this so that it moves the right wing down)
            set_pwm(RIGHT, UP, 0);
        }
    }
}
//    return duty;} //only used for testing if duty cycle values can be printed 
void init_spi()
{
    //init spi bus
    common_hal_busio_spi_construct(&spi, SCK, MOSI,MISO);

    //init chip select pins
    common_hal_digitalio_digitalinout_construct(&sensor_left.cs, CS_LEFT);
    common_hal_digitalio_digitalinout_switch_to_output(&sensor_left.cs, true, DRIVE_MODE_PUSH_PULL);
    common_hal_digitalio_digitalinout_construct(&sensor_center.cs, CS_CENTER);
    common_hal_digitalio_digitalinout_switch_to_output(&sensor_center.cs, true, DRIVE_MODE_PUSH_PULL);
    common_hal_digitalio_digitalinout_construct(&sensor_right.cs, CS_RIGHT);
    common_hal_digitalio_digitalinout_switch_to_output(&sensor_right.cs, true, DRIVE_MODE_PUSH_PULL);


    //construct spidevices
    //These are the default values from shared-bindings/adafruit_bus_device/SPIDevice.c
    uint32_t baudrate=100000;
    uint8_t polarity = 0;
    uint8_t phase = 0;
    uint8_t extra_clocks = 0;
    common_hal_adafruit_bus_device_spidevice_construct(&sensor_left.sensor, &spi, &sensor_left.cs,baudrate, polarity, phase, extra_clocks);
    common_hal_adafruit_bus_device_spidevice_construct(&sensor_center.sensor, &spi, &sensor_center.cs,baudrate, polarity, phase, extra_clocks);
    common_hal_adafruit_bus_device_spidevice_construct(&sensor_right.sensor, &spi, &sensor_right.cs,baudrate, polarity, phase, extra_clocks);

    initSensors(&sensor_left);
    initSensors(&sensor_center);
    initSensors(&sensor_right);
}

//Writes to SPI bus, make sure the device has been "entered" into
static void write(uint8_t *buffer, size_t length)
{
    common_hal_busio_spi_write(&spi, buffer, length);
}

//Reads from SPI bus into buffer
static void readinto(uint8_t *buffer, size_t length)
{
    common_hal_busio_spi_read(&spi, buffer, length, 0);
}

//See https://github.com/adafruit/Adafruit_CircuitPython_LIS3DH/blob/master/adafruit_lis3dh.py
static void *_read_register (spi_sensor_t *device, uint8_t reg, size_t length)
{
    if (length == 1)
    {
        device->gbuf[0] = (reg | 0x80) & 0xFF;
    }
    else
    {
        device->gbuf[0] = (reg | 0xC0) & 0xFF;
    }
    //Enter before beginning the operation
    common_hal_adafruit_bus_device_spidevice_enter(&device->sensor);
    write(device->gbuf, 1);
    readinto(device->gbuf, length);
    common_hal_adafruit_bus_device_spidevice_exit(&device->sensor);
    //Exit after completing the operation
    return device->gbuf;
}
//Uncomment when needed since circuitpython doesn't compile with warnings
static void _write_register_byte(spi_sensor_t *device, uint8_t reg, uint8_t value)
{
    device->gbuf[0] = reg & 0x7F;
    device->gbuf[1] = value & 0xFF;
    common_hal_adafruit_bus_device_spidevice_enter(&device->sensor);
    write(device->gbuf, 2);
    common_hal_adafruit_bus_device_spidevice_exit(&device->sensor);
}


static uint8_t _read_register_byte(spi_sensor_t *device, uint8_t reg)
{
    return *(int *)_read_register(device, reg, 1);
}

//Tests the SPI bus, in the case of the LIS3DH this should print 0x33
//If this prints 0xFF, check the pins/physical connection
void test()
{
    uint8_t id = _read_register_byte(&sensor_center, 0x0F);
    printf("ID: 0x%X\n", id);

}

void initSensors(spi_sensor_t *device) {

    // Check device ID
    uint8_t device_id = _read_register_byte(device, _REG_WHOAMI);
    if (device_id != 0x33) {
      printf("Failed to find LIS3DH on chip select");
    }
    // Reboot
    _write_register_byte(device, _REG_CTRL5, 0x80);
    // TODO: wait/sleep for 5 ms
    // Enable all axes, normal mode.
    _write_register_byte(device, _REG_CTRL1, 0x07);
    // Set 400Hz data rate.
    set_data_rate(device, DATARATE_400_HZ);
    // High res & BDU enabled.
    _write_register_byte(device, _REG_CTRL4, 0x88);
    // Enable ADCs.
    _write_register_byte(device, _REG_TEMPCFG, 0x80);
    // Latch interrupt for INT1
    _write_register_byte(device, _REG_CTRL5, 0x08);
}

uint8_t get_data_rate(spi_sensor_t *device) {
/* The data rate of the accelerometer.  Can be DATA_RATE_400_HZ, DATA_RATE_200_HZ,
    DATA_RATE_100_HZ, DATA_RATE_50_HZ, DATA_RATE_25_HZ, DATA_RATE_10_HZ,
    DATA_RATE_1_HZ, DATA_RATE_POWERDOWN, DATA_RATE_LOWPOWER_1K6HZ, or
    DATA_RATE_LOWPOWER_5KHZ. */
    uint8_t ctl1 = _read_register_byte(device, _REG_CTRL1);
    return (ctl1 >> 4) & 0x0F;
}

void set_data_rate(spi_sensor_t *device, uint8_t rate) {
    uint8_t ctl1 = _read_register_byte(device, _REG_CTRL1);
    ctl1 &= ~(0xF0);
    ctl1 |= rate << 4;
    _write_register_byte(device, _REG_CTRL1, ctl1);
}

uint8_t get_range(spi_sensor_t *device) {
  /* The range of the accelerometer.  Can be RANGE_2_G, RANGE_4_G, RANGE_8_G, or
      RANGE_16_G. */
    uint8_t ctl4 = _read_register_byte(device, _REG_CTRL4);
    return (ctl4 >> 4) & 0x03;
}

void set_range(spi_sensor_t *device, uint8_t range_value) {
    uint8_t ctl4 = _read_register_byte(device, _REG_CTRL4);
    ctl4 &= ~0x30;
    ctl4 |= range_value << 4;
    _write_register_byte(device, _REG_CTRL4, ctl4);
}

acceleration read_accelerometer(spi_sensor_t *device) {
    float divider = 1;
    uint8_t accel_range = get_range(device);
    acceleration accel;
    switch(accel_range) {
      case RANGE_16_G:
        divider = 1365;
        break;
      case RANGE_8_G:
        divider = 4096;
        break;
      case RANGE_4_G:
        divider = 8190;
        break;
      case RANGE_2_G:
        divider = 16380;
        break;
    }

    // Values are stored in two registers as left-justified two's complement
    // read into int16_t as these are stored as two's complement
    // read high and low bytes, and shift right 4 (12b high-resolution mode)
    int16_t raw_x = _read_register_byte(device, _REG_OUT_X_H | 0x80);
    raw_x <<= 8;
    raw_x |= _read_register_byte(device, _REG_OUT_X_L | 0x80);
    raw_x >>= 4;

    int16_t raw_y = _read_register_byte(device, _REG_OUT_Y_H | 0x80);
    raw_y <<= 8;
    raw_y |= _read_register_byte(device, _REG_OUT_Y_L | 0x80);
    raw_y >>= 4;

    int16_t raw_z = _read_register_byte(device, _REG_OUT_Z_H | 0x80);
    raw_z <<= 8;
    raw_z |= _read_register_byte(device, _REG_OUT_Z_L | 0x80);
    raw_z >>= 4;

    // convert from Gs to m / s ^ 2 and adjust for the range
    accel.x = ((float)raw_x / divider) * STANDARD_GRAVITY;
    accel.y = ((float)raw_y / divider) * STANDARD_GRAVITY;
    accel.z = ((float)raw_z / divider) * STANDARD_GRAVITY;

    return accel;
}
