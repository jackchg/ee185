/*
 * This file is part of the MicroPython project, http://micropython.org/
 *
 * The MIT License (MIT)
 *
 * Copyright (c) 2018 Scott Shawcroft for Adafruit Industries */

#include "ports/atmel-samd/peripherals/samd/timers.h"
#include "py/mphal.h"
#include "supervisor/port.h"
#include "supervisor/shared/tick.h"
#include "py/runtime.h"
#include "shared-module/motor_control/__init__.h"
#include "ports/atmel-samd/asf4/samd51/driver_init.h"
#include <stdio.h>
#include "ports/atmel-samd/asf4/samd51/hal/include/hpl_timer.h"
#include "ports/atmel-samd/asf4/samd51/hpl/rtc/hpl_rtc_base.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"
#include "ports/atmel-samd/timer_handler.h"
int static state = 0;



// These define's must be placed at the beginning before #include "SAMDTimerInterrupt.h"
// _TIMERINTERRUPT_LOGLEVEL_ from 0 to 4
// Don't define _TIMERINTERRUPT_LOGLEVEL_ > 0. Only for special ISR debugging only. Can hang the system.
// Don't define TIMER_INTERRUPT_DEBUG > 2. Only for special ISR debugging only. Can hang the system.


void shared_module_control_motor_construct(control_motor_obj_t* self) {
  self->deinited = 0;
}

bool shared_module_control_motor_deinited(control_motor_obj_t* self) {
  return self->deinited;
    }
     
void shared_module_control_motor_deinit(control_motor_obj_t* self) {
      self->deinited = 1;
    }

/*This is the function that Python's control.tasks(degrees) is linked to in shared-bindings.
Does the following:
    1. For the first call, this function calls common_hal_motor_construct() to choose a timer for the interrupt 
    2. When called again (or multiple times), it does the following:
        -Run the PID calculation ( common_hal_motor_PID() )
        -Changes the set angle for the wing ( control_set_degrees(degrees) ) */

void interrupt_work(void){
  if (state == 0){
    common_hal_motor_construct();
    state = 1;
  }
  else{
    common_hal_motor_PID();
    control_set_degrees(degrees);
  }
}

  

