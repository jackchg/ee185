#ifndef MICROPY_INCLUDED_SHARED_BINDINGS_EE285___INIT___H
#define MICROPY_INCLUDED_SHARED_BINDINGS_EE285___INIT___H
#include "py/obj.h"
#include "ports/atmel-samd/timer_handler.h"
#include "shared-module/motor_control/__init__.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"


extern const mp_obj_type_t control_motor_type;
extern void shared_module_control_motor_construct(control_motor_obj_t* self);
extern void interrupt_work(void);
extern void set_pwm(bool left, bool up, float duty);
extern void init_spi(void);
extern void init_spidevice(void);
extern void test(void);
extern void shared_module_control_motor_deinit(control_motor_obj_t* self);
extern bool shared_module_control_motor_deinited(control_motor_obj_t* self);

#endif  // MICROPY_INCLUDED_SHARED_BINDINGS_EE285___INIT___H