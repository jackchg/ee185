#include <stdint.h>

#include "py/obj.h"
#include "py/runtime.h"

#include "shared-bindings/motor_control/__init__.h"

#include <stdint.h>
#include <string.h>
#include "lib/utils/context_manager_helpers.h"
#include "py/objproperty.h"
#include "py/runtime.h"
#include "py/runtime0.h"
#include "shared-bindings/util.h"
#include "ports/atmel-samd/common-hal/motor_control/Control.h"



//| .. currentmodule:: motor_control

int degrees;

STATIC mp_obj_t control_motor_make_new(const mp_obj_type_t *type, size_t n_args, const mp_obj_t *pos_args, mp_map_t *kw_args) {
    mp_arg_check_num(n_args, kw_args, 0, 0, true);
    control_motor_obj_t *self = m_new_obj(control_motor_obj_t);
    self->base.type = &control_motor_type;
    shared_module_control_motor_construct(self);
    return MP_OBJ_FROM_PTR(self);
}

STATIC mp_obj_t control_motor_deinit(mp_obj_t self_in) {
  shared_module_control_motor_deinit(self_in);
  return mp_const_none;
}
STATIC MP_DEFINE_CONST_FUN_OBJ_1(control_motor_deinit_obj, control_motor_deinit);


STATIC mp_obj_t control_motor_obj___exit__(size_t n_args, const mp_obj_t *args) {
  shared_module_control_motor_deinit(args[0]);
  return mp_const_none;
}

STATIC MP_DEFINE_CONST_FUN_OBJ_VAR_BETWEEN(control_motor___exit___obj, 4, 4, control_motor_obj___exit__);

// This is the function that runs the PID loop and changes the duty cycle value. It gets called in the python script.

STATIC mp_obj_t control_motor_obj_tasks(size_t n_args, const mp_obj_t *pos_args, mp_map_t *kw_args) {
  enum {ARG_degrees};
  static const mp_arg_t allowed_args[] = {
    { MP_QSTR_degrees, MP_ARG_REQUIRED | MP_ARG_INT, {.u_int= 0 }}, //checks if the set angle from control.tasks(degrees) is an integer
  };
  mp_arg_val_t args[MP_ARRAY_SIZE(allowed_args)];
  mp_arg_parse_all(n_args, pos_args, kw_args, MP_ARRAY_SIZE(allowed_args), allowed_args, args); //parses Micropython array to obtain set angle
  degrees = args[ARG_degrees].u_int;
  interrupt_work(); //Function that runs PID loop. Found in "shared-module/motor_control/__init__.c"
  return mp_const_none;
}

MP_DEFINE_CONST_FUN_OBJ_KW(control_motor_tasks_obj, 1, control_motor_obj_tasks);

// This is the function that initializes the spi bus. Called once in python script.

STATIC mp_obj_t control_motor_obj_init_spi(void) {
  init_spi();
  return mp_const_none;
}

MP_DEFINE_CONST_FUN_OBJ_0(control_init_spi_obj, control_motor_obj_init_spi);

STATIC mp_obj_t control_motor_obj_test(void) {
  test();
  return mp_const_none;
}

MP_DEFINE_CONST_FUN_OBJ_0(control_test_obj, control_motor_obj_test);

STATIC mp_obj_t control_motor_obj_set_pwm(mp_obj_t left, mp_obj_t up, mp_obj_t duty) {
  bool cleft = mp_obj_get_int(left);
  bool cup = mp_obj_get_int(up);
  float cduty = mp_obj_get_float(duty);

  set_pwm(cleft, cup, cduty);
  return mp_const_none;
}

MP_DEFINE_CONST_FUN_OBJ_3(control_set_pwm_obj, control_motor_obj_set_pwm);

STATIC const mp_rom_map_elem_t control_module_globals_table[] = {
    { MP_ROM_QSTR(MP_QSTR___name__), MP_ROM_QSTR(MP_QSTR_control) },
    { MP_ROM_QSTR(MP_QSTR_motor), MP_ROM_PTR(&control_motor_type) },
    { MP_ROM_QSTR(MP_QSTR___deinit__), MP_ROM_PTR(&control_motor_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR___exit__), MP_ROM_PTR(&control_motor___exit___obj) },
    { MP_ROM_QSTR(MP_QSTR_tasks), MP_ROM_PTR(&control_motor_tasks_obj) },
    { MP_ROM_QSTR(MP_QSTR_pwm), MP_ROM_PTR(&control_set_pwm_obj) },
    { MP_ROM_QSTR(MP_QSTR_init_spi), MP_ROM_PTR(&control_init_spi_obj) },
    { MP_ROM_QSTR(MP_QSTR_test), MP_ROM_PTR(&control_test_obj) }
};

STATIC MP_DEFINE_CONST_DICT(control_module_globals, control_module_globals_table);

const mp_obj_module_t control_module = {
    .base = { &mp_type_module },
    .globals = (mp_obj_dict_t*)&control_module_globals,
};

STATIC const mp_rom_map_elem_t control_motor_locals_dict_table[] = {
    // Methods
    { MP_ROM_QSTR(MP_QSTR_deinit), MP_ROM_PTR(&control_motor_deinit_obj) },
    { MP_ROM_QSTR(MP_QSTR___exit__), MP_ROM_PTR(&control_motor___exit___obj) },
    
};
STATIC MP_DEFINE_CONST_DICT(control_motor_locals_dict, control_motor_locals_dict_table);


const mp_obj_type_t control_motor_type = {
    { &mp_type_type },
    .name = MP_QSTR_Meaning, //TODO: Figure out what this is
    .make_new = control_motor_make_new,
    .locals_dict = (mp_obj_dict_t*)&control_motor_locals_dict,
};