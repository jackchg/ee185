#ifndef MICROPY_INCLUDED_SHARED_BINDINGS_EE285___INIT___H
#define MICROPY_INCLUDED_SHARED_BINDINGS_EE285___INIT___H

#include "py/obj.h"
#include "shared-module/ee285/__init__.h"

extern void shared_module_ee285_test(void);
extern void shared_module_ee285_body_leds(mp_obj_t rgb);
extern void shared_module_ee285_wing_leds(int side, mp_obj_t rgb1, mp_obj_t rgb2, mp_obj_t rgb3);
extern void shared_module_ee285_wing_angle(int side, float angle);


#endif  // MICROPY_INCLUDED_SHARED_BINDINGS_EE285___INIT___H
