// Server side C/C++ program from geeksforgeeks.org
#include <unistd.h> 
#include <stdio.h> 
#include <sys/socket.h> 
#include <stdlib.h> 
#include <netinet/in.h> 
#include <string.h> 
#include <poll.h>
#include <stdbool.h>
#include <mutex>

#include "flight_server.h"

#define PORT 8080
#define NUM_FLYERS 76
#define BODYLED 1
#define WINGLED 2
#define WINGANGLE 3
#define LEFT 1
#define RIGHT 0

struct flight_flyer flyerStates[NUM_FLYERS];
std::mutex flyerLock;

int flight_server_num_flyers() {
  return NUM_FLYERS;
}

struct flight_flyer* flight_server_get_flyer(int which) {
  if (which < 0 || which >= NUM_FLYERS) {
    return NULL;
  } else {
    return &(flyerStates[which]);
  }
}

int flight_server_start(){ 

    int server_fd, valread; 
    struct sockaddr_in address; 
    int opt = 1; 
    int addrlen = sizeof(address); 
    
       
    // Creating socket file descriptor 
    if ((server_fd = socket(AF_INET, SOCK_STREAM, 0)) == 0) 
    { 
        perror("socket failed"); 
        exit(EXIT_FAILURE); 
    } 
       
    // Forcefully attaching socket to the port 8080 
    if (setsockopt(server_fd, SOL_SOCKET, SO_REUSEADDR | SO_REUSEPORT, 
                                                  &opt, sizeof(opt))) 
    { 
        perror("setsockopt"); 
        exit(EXIT_FAILURE); 
    } 
    address.sin_family = AF_INET; 
    address.sin_addr.s_addr = INADDR_ANY; 
    address.sin_port = htons( PORT ); 
       
    // Forcefully attaching socket to the port 8080 
    if (bind(server_fd, (struct sockaddr *)&address,  
                                 sizeof(address))<0) 
    { 
        perror("bind failed"); 
        exit(EXIT_FAILURE); 
    } 
    //create 64 threads to listen to each FF
    if (listen(server_fd, 3) < 0) 
    { 
        perror("listen"); 
        exit(EXIT_FAILURE); 
    } 
    //pollfd contains one extra fd for the server to listen to new connections
    struct pollfd fds[NUM_FLYERS+1];
    for(int i = 0; i < NUM_FLYERS; i++){
        int tempfd;
        if ((tempfd = accept(server_fd, (struct sockaddr *)&address,  
                           (socklen_t*)&addrlen))<0) 
        { 
            perror("accept"); 
            exit(EXIT_FAILURE); 
        } 
        int id;
        char message_recieved[1024] = {0}; 
        //reads in message to id the connected FF
        valread = read(tempfd, message_recieved, 1024);
        sscanf(message_recieved,"%d",&id);

        fds[id].fd = tempfd;
        fds[i].events = POLLIN;

    }
    fds[NUM_FLYERS].fd = server_fd;
    fds[NUM_FLYERS].events = POLLIN;
    
    int total = NUM_FLYERS;
    flyerLock.lock();
    while(total != 0){
        bool change = false;
        flyerLock.unlock();
        int num = poll(fds,NUM_FLYERS+1,-1);
        flyerLock.lock();
        if(num == -1)
            perror("poll");
        else if(num == 0)
            perror("timeout");
        //loop over all FF + listen of connections
        for(int i = 0; i < NUM_FLYERS+1; i++){
            if (fds[i].revents & POLLIN) {
                if(i == NUM_FLYERS){
                    int tempfd;
                    if ((tempfd = accept(server_fd, (struct sockaddr *)&address,  
                                       (socklen_t*)&addrlen))<0) 
                    { 
                        perror("accept"); 
                        exit(EXIT_FAILURE); 
                    } 
                    int id;
                    char message_recieved[1024] = {0}; 
                    //reads in message to id the connected FF
                    valread = read(tempfd, message_recieved, 1024);
                    sscanf(message_recieved,"%d",&id);
                    //temperary code where simulator can request the flyer state
                    if(id == -1){
                        for(int j = 0; j < NUM_FLYERS; j++){
                            char output[1024] = {0};
                            sprintf(output,"\nFlyer:%d:%d\nBody:(%d,%d,%d)\nLeftWing:(%d,%d,%d):"
                                "(%d,%d,%d):(%d,%d,%d):Angle:%lf\nRightWing:(%d,%d,%d):"
                                "(%d,%d,%d):(%d,%d,%d):Angle:%lf\n\n",i,flyerStates[j].number,
                                    flyerStates[j].body.red,flyerStates[j].body.green,
                                    flyerStates[j].body.blue,flyerStates[j].leftWing.node1.red,
                                    flyerStates[j].leftWing.node1.green,flyerStates[j].leftWing.node1.blue,
                                    flyerStates[j].leftWing.node2.red,flyerStates[j].leftWing.node2.green,
                                    flyerStates[j].leftWing.node2.blue,flyerStates[j].leftWing.node3.red,
                                    flyerStates[j].leftWing.node3.green,flyerStates[j].leftWing.node3.blue,
                                    flyerStates[j].leftWing.angle,flyerStates[j].rightWing.node1.red,
                                    flyerStates[j].rightWing.node1.green,flyerStates[j].rightWing.node1.blue,
                                    flyerStates[j].rightWing.node2.red,flyerStates[j].rightWing.node2.green,
                                    flyerStates[j].rightWing.node2.blue,flyerStates[j].rightWing.node3.red,
                                    flyerStates[j].rightWing.node3.green,flyerStates[j].rightWing.node3.blue,
                                    flyerStates[j].rightWing.angle);
                            send(tempfd , output , strlen(output) , 0 ); 
                        }
                        send(tempfd, "EOF",strlen("EOF"),0);
                        close(tempfd);
                    }else{
                        fds[id].fd = tempfd;
                        //replaces old FF connection with new
                        printf("Opened FF %d\n",id);
                    }
                    
                    break;
                }
                char message_recieved[1024] = {0}; 
                const char *message_send = "Hello from server!!"; 
                //close directory when you cant read anything
                valread = read( fds[i].fd , message_recieved, 1024);
                if(valread <= 0){
                    fds[i].fd = -1;
                    printf("Closed FF %d\n",i);
                    continue;
                }
                int temploc;
                int tempCommand;
                char tempRest[100];
                sscanf(message_recieved,"%d:%d:%s",&temploc,&tempCommand,tempRest);
                flyerStates[temploc].number = temploc;
                change = true;
                if(tempCommand == BODYLED){
                    struct flight_rgb rgbTemp;
                    sscanf(tempRest,"%d,%d,%d",&rgbTemp.red,&rgbTemp.green,&rgbTemp.blue);
                    flyerStates[temploc].body = rgbTemp;
                }
                else if(tempCommand == WINGLED){
                    struct flight_rgb rgbNode1Temp;
                    struct flight_rgb rgbNode2Temp;
                    struct flight_rgb rgbNode3Temp;
                    bool direction;
                    sscanf(tempRest,"%d:%d,%d,%d:%d,%d,%d:%d,%d,%d",
                            (int*)&direction,&rgbNode1Temp.red,&rgbNode1Temp.green,
                            &rgbNode1Temp.blue,&rgbNode2Temp.red,&rgbNode2Temp.green,
                            &rgbNode2Temp.blue,&rgbNode3Temp.red,&rgbNode3Temp.green,
                            &rgbNode3Temp.blue);
                    if (direction == LEFT){
                        flyerStates[temploc].leftWing.node1 = rgbNode1Temp;
                        flyerStates[temploc].leftWing.node2 = rgbNode2Temp;
                        flyerStates[temploc].leftWing.node3 = rgbNode3Temp;
                    }else{
                        flyerStates[temploc].rightWing.node1 = rgbNode1Temp;
                        flyerStates[temploc].rightWing.node2 = rgbNode2Temp;
                        flyerStates[temploc].rightWing.node3 = rgbNode3Temp;
                    }
                }else if(tempCommand == WINGANGLE){
                    bool direction;
                    double angle;
                    sscanf(tempRest,"%d:%lf",(int*)&direction,&angle);
                    if(direction == LEFT){
                        flyerStates[temploc].leftWing.angle = angle;
                    }else{
                        flyerStates[temploc].rightWing.angle = angle;
                    }
                }
                //printf("Recieved message Len: %d\n",valread);
                printf("Message Recieved: %s\n",message_recieved); 
                // send(fds[i].fd , message_send , strlen(message_send) , 0 );
                // //printf("Sent message Len: %lu\n",strlen(message_send)); 
                //printf("Message sent: %s\n",message_send); 
            }
        }
        if(change){
            for(int i = 0; i < NUM_FLYERS; i++){
                printf("\nFlyer:%d:%d\nBody:(%d,%d,%d)\nLeftWing:(%d,%d,%d):"
                    "(%d,%d,%d):(%d,%d,%d):Angle:%lf\nRightWing:(%d,%d,%d):"
                    "(%d,%d,%d):(%d,%d,%d):Angle:%lf\n\n",i,flyerStates[i].number,
                        flyerStates[i].body.red,flyerStates[i].body.green,
                        flyerStates[i].body.blue,flyerStates[i].leftWing.node1.red,
                        flyerStates[i].leftWing.node1.green,flyerStates[i].leftWing.node1.blue,
                        flyerStates[i].leftWing.node2.red,flyerStates[i].leftWing.node2.green,
                        flyerStates[i].leftWing.node2.blue,flyerStates[i].leftWing.node3.red,
                        flyerStates[i].leftWing.node3.green,flyerStates[i].leftWing.node3.blue,
                        flyerStates[i].leftWing.angle,flyerStates[i].rightWing.node1.red,
                        flyerStates[i].rightWing.node1.green,flyerStates[i].rightWing.node1.blue,
                        flyerStates[i].rightWing.node2.red,flyerStates[i].rightWing.node2.green,
                        flyerStates[i].rightWing.node2.blue,flyerStates[i].rightWing.node3.red,
                        flyerStates[i].rightWing.node3.green,flyerStates[i].rightWing.node3.blue,
                        flyerStates[i].rightWing.angle);
            }
        }
            
    }
    return 0; 
} 
