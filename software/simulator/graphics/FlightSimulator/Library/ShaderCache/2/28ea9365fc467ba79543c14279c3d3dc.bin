<Q                         POINT_COOKIE    7  #ifdef VERTEX
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec4 hlslcc_mtx4x4unity_ObjectToWorld[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToObject[4];
uniform 	vec4 unity_WorldTransformParams;
uniform 	vec4 hlslcc_mtx4x4unity_MatrixVP[4];
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	vec4 _MainTex_ST;
uniform 	vec4 _Mask_ST;
uniform 	vec4 _Noise_ST;
uniform 	vec4 _BumpMap_ST;
in  vec4 in_POSITION0;
in  vec4 in_TANGENT0;
in  vec3 in_NORMAL0;
in  vec4 in_TEXCOORD0;
out vec4 vs_TEXCOORD0;
out vec4 vs_TEXCOORD1;
out vec3 vs_TEXCOORD2;
out vec3 vs_TEXCOORD3;
out vec3 vs_TEXCOORD4;
out vec3 vs_TEXCOORD5;
out vec3 vs_TEXCOORD6;
out vec4 vs_TEXCOORD7;
vec4 u_xlat0;
vec4 u_xlat1;
vec4 u_xlat2;
vec3 u_xlat3;
float u_xlat13;
void main()
{
    u_xlat0 = in_POSITION0.yyyy * hlslcc_mtx4x4unity_ObjectToWorld[1];
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[0] * in_POSITION0.xxxx + u_xlat0;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[2] * in_POSITION0.zzzz + u_xlat0;
    u_xlat1 = u_xlat0 + hlslcc_mtx4x4unity_ObjectToWorld[3];
    u_xlat2 = u_xlat1.yyyy * hlslcc_mtx4x4unity_MatrixVP[1];
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[0] * u_xlat1.xxxx + u_xlat2;
    u_xlat2 = hlslcc_mtx4x4unity_MatrixVP[2] * u_xlat1.zzzz + u_xlat2;
    gl_Position = hlslcc_mtx4x4unity_MatrixVP[3] * u_xlat1.wwww + u_xlat2;
    vs_TEXCOORD0.xy = in_TEXCOORD0.xy * _MainTex_ST.xy + _MainTex_ST.zw;
    vs_TEXCOORD0.zw = in_TEXCOORD0.xy * _Mask_ST.xy + _Mask_ST.zw;
    vs_TEXCOORD1.xy = in_TEXCOORD0.xy * _Noise_ST.xy + _Noise_ST.zw;
    vs_TEXCOORD1.zw = in_TEXCOORD0.xy * _BumpMap_ST.xy + _BumpMap_ST.zw;
    u_xlat1.y = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[0].xyz);
    u_xlat1.z = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[1].xyz);
    u_xlat1.x = dot(in_NORMAL0.xyz, hlslcc_mtx4x4unity_WorldToObject[2].xyz);
    u_xlat13 = dot(u_xlat1.xyz, u_xlat1.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat1.xyz = vec3(u_xlat13) * u_xlat1.xyz;
    u_xlat2.xyz = in_TANGENT0.yyy * hlslcc_mtx4x4unity_ObjectToWorld[1].yzx;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[0].yzx * in_TANGENT0.xxx + u_xlat2.xyz;
    u_xlat2.xyz = hlslcc_mtx4x4unity_ObjectToWorld[2].yzx * in_TANGENT0.zzz + u_xlat2.xyz;
    u_xlat13 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat13 = inversesqrt(u_xlat13);
    u_xlat2.xyz = vec3(u_xlat13) * u_xlat2.xyz;
    u_xlat3.xyz = u_xlat1.xyz * u_xlat2.xyz;
    u_xlat3.xyz = u_xlat1.zxy * u_xlat2.yzx + (-u_xlat3.xyz);
    u_xlat13 = in_TANGENT0.w * unity_WorldTransformParams.w;
    u_xlat3.xyz = vec3(u_xlat13) * u_xlat3.xyz;
    vs_TEXCOORD2.y = u_xlat3.x;
    vs_TEXCOORD2.x = u_xlat2.z;
    vs_TEXCOORD2.z = u_xlat1.y;
    vs_TEXCOORD3.x = u_xlat2.x;
    vs_TEXCOORD4.x = u_xlat2.y;
    vs_TEXCOORD3.z = u_xlat1.z;
    vs_TEXCOORD4.z = u_xlat1.x;
    vs_TEXCOORD3.y = u_xlat3.y;
    vs_TEXCOORD4.y = u_xlat3.z;
    vs_TEXCOORD5.xyz = hlslcc_mtx4x4unity_ObjectToWorld[3].xyz * in_POSITION0.www + u_xlat0.xyz;
    u_xlat0 = hlslcc_mtx4x4unity_ObjectToWorld[3] * in_POSITION0.wwww + u_xlat0;
    u_xlat1.xyz = u_xlat0.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat1.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * u_xlat0.xxx + u_xlat1.xyz;
    u_xlat0.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * u_xlat0.zzz + u_xlat1.xyz;
    vs_TEXCOORD6.xyz = hlslcc_mtx4x4unity_WorldToLight[3].xyz * u_xlat0.www + u_xlat0.xyz;
    vs_TEXCOORD7 = vec4(0.0, 0.0, 0.0, 0.0);
    return;
}

#endif
#ifdef FRAGMENT
#version 150
#extension GL_ARB_explicit_attrib_location : require
#ifdef GL_ARB_shader_bit_encoding
#extension GL_ARB_shader_bit_encoding : enable
#endif

#define HLSLCC_ENABLE_UNIFORM_BUFFERS 1
#if HLSLCC_ENABLE_UNIFORM_BUFFERS
#define UNITY_UNIFORM
#else
#define UNITY_UNIFORM uniform
#endif
#define UNITY_SUPPORTS_UNIFORM_LOCATION 1
#if UNITY_SUPPORTS_UNIFORM_LOCATION
#define UNITY_LOCATION(x) layout(location = x)
#define UNITY_BINDING(x) layout(binding = x, std140)
#else
#define UNITY_LOCATION(x)
#define UNITY_BINDING(x) layout(std140)
#endif
uniform 	vec3 _WorldSpaceCameraPos;
uniform 	vec4 _WorldSpaceLightPos0;
uniform 	vec4 unity_OcclusionMaskSelector;
uniform 	vec4 unity_ProbeVolumeParams;
uniform 	vec4 hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[4];
uniform 	vec3 unity_ProbeVolumeSizeInv;
uniform 	vec3 unity_ProbeVolumeMin;
uniform 	vec4 _LightColor0;
uniform 	vec4 hlslcc_mtx4x4unity_WorldToLight[4];
uniform 	vec4 _ColorRamp_ST;
uniform 	float _Glossiness;
uniform 	float _Metallic;
uniform 	float _Blend;
uniform 	float _BumpPower;
uniform 	float _Distortion;
uniform 	float _Hue;
uniform 	float _Saturation;
uniform 	float _Brightness;
uniform 	float _Contrast;
UNITY_LOCATION(0) uniform  sampler2D _MainTex;
UNITY_LOCATION(1) uniform  sampler2D _Noise;
UNITY_LOCATION(2) uniform  sampler2D _BumpMap;
UNITY_LOCATION(3) uniform  sampler2D _Mask;
UNITY_LOCATION(4) uniform  sampler2D _ColorRamp;
UNITY_LOCATION(5) uniform  sampler2D _LightTextureB0;
UNITY_LOCATION(6) uniform  samplerCube _LightTexture0;
UNITY_LOCATION(7) uniform  sampler3D unity_ProbeVolumeSH;
in  vec4 vs_TEXCOORD0;
in  vec4 vs_TEXCOORD1;
in  vec3 vs_TEXCOORD2;
in  vec3 vs_TEXCOORD3;
in  vec3 vs_TEXCOORD4;
in  vec3 vs_TEXCOORD5;
layout(location = 0) out vec4 SV_Target0;
vec3 u_xlat0;
vec3 u_xlat1;
vec3 u_xlat2;
vec4 u_xlat3;
vec4 u_xlat4;
vec4 u_xlat5;
vec4 u_xlat6;
vec3 u_xlat7;
float u_xlat8;
float u_xlat9;
float u_xlat10;
vec3 u_xlat12;
vec3 u_xlat13;
float u_xlat14;
float u_xlat21;
bool u_xlatb21;
float u_xlat22;
bool u_xlatb22;
float u_xlat23;
float u_xlat24;
float u_xlat25;
void main()
{
    u_xlat0.xyz = (-vs_TEXCOORD5.xyz) + _WorldSpaceLightPos0.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat1.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat2.xyz = (-vs_TEXCOORD5.xyz) + _WorldSpaceCameraPos.xyz;
    u_xlat22 = dot(u_xlat2.xyz, u_xlat2.xyz);
    u_xlat22 = inversesqrt(u_xlat22);
    u_xlat2.xyz = vec3(u_xlat22) * u_xlat2.xyz;
    u_xlat3.xyz = u_xlat2.yyy * vs_TEXCOORD3.xyz;
    u_xlat3.xyz = vs_TEXCOORD2.xyz * u_xlat2.xxx + u_xlat3.xyz;
    u_xlat3.xyz = vs_TEXCOORD4.xyz * u_xlat2.zzz + u_xlat3.xyz;
    u_xlat4 = texture(_MainTex, vs_TEXCOORD0.xy);
    u_xlat5 = texture(_Noise, vs_TEXCOORD1.xy);
    u_xlat6 = texture(_BumpMap, vs_TEXCOORD1.zw);
    u_xlat6.x = u_xlat6.w * u_xlat6.x;
    u_xlat6.xy = u_xlat6.xy * vec2(2.0, 2.0) + vec2(-1.0, -1.0);
    u_xlat22 = dot(u_xlat6.xy, u_xlat6.xy);
    u_xlat22 = min(u_xlat22, 1.0);
    u_xlat22 = (-u_xlat22) + 1.0;
    u_xlat22 = sqrt(u_xlat22);
    u_xlat6.z = u_xlat22 / _BumpPower;
    u_xlat22 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlat22 = inversesqrt(u_xlat22);
    u_xlat12.xyz = vec3(u_xlat22) * u_xlat6.xyz;
    u_xlat22 = dot(u_xlat3.xyz, u_xlat3.xyz);
    u_xlat22 = inversesqrt(u_xlat22);
    u_xlat6.xyz = vec3(u_xlat22) * u_xlat3.xyz;
    u_xlat22 = dot(u_xlat6.xyz, u_xlat12.xyz);
    u_xlat23 = u_xlat5.x * _Distortion;
    u_xlat6 = texture(_Mask, vs_TEXCOORD0.zw);
    u_xlat3.xy = u_xlat3.xy * _ColorRamp_ST.xy;
    u_xlat3.xy = vec2(u_xlat22) * vec2(u_xlat23) + u_xlat3.xy;
    u_xlat3.xy = u_xlat3.xy + _ColorRamp_ST.zw;
    u_xlat3 = texture(_ColorRamp, u_xlat3.xy);
    u_xlat3.xyz = u_xlat6.xyz * u_xlat3.xyz;
    u_xlat6.xyz = (-u_xlat6.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat6.xyz = u_xlat4.xyz * u_xlat6.xyz;
    u_xlat3.xyz = max(u_xlat3.xyz, u_xlat6.xyz);
    u_xlat22 = _Saturation + _Saturation;
    u_xlat23 = _Brightness * 2.0 + -1.0;
    u_xlat24 = _Contrast + _Contrast;
    u_xlat25 = _Hue * 6.28318548;
    u_xlat5.x = sin(u_xlat25);
    u_xlat6.x = cos(u_xlat25);
    u_xlat13.xyz = u_xlat3.zxy * vec3(0.57735002, 0.57735002, 0.57735002);
    u_xlat13.xyz = u_xlat3.zxy * vec3(0.57735002, 0.57735002, 0.57735002) + (-u_xlat13.zxy);
    u_xlat13.xyz = u_xlat5.xxx * u_xlat13.xyz;
    u_xlat13.xyz = u_xlat3.xyz * u_xlat6.xxx + u_xlat13.xyz;
    u_xlat3.x = dot(vec3(0.57735002, 0.57735002, 0.57735002), u_xlat3.xyz);
    u_xlat3.x = u_xlat3.x * 0.57735002;
    u_xlat10 = (-u_xlat6.x) + 1.0;
    u_xlat3.xyz = u_xlat3.xxx * vec3(u_xlat10) + u_xlat13.xyz;
    u_xlat3.xyz = u_xlat3.xyz + vec3(-0.5, -0.5, -0.5);
    u_xlat3.xyz = u_xlat3.xyz * vec3(u_xlat24) + vec3(0.5, 0.5, 0.5);
    u_xlat3.xyz = vec3(u_xlat23) + u_xlat3.xyz;
    u_xlat23 = dot(u_xlat3.xyz, vec3(0.389999986, 0.589999974, 0.109999999));
    u_xlat3.xyz = (-vec3(u_xlat23)) + u_xlat3.xyz;
    u_xlat3.xyz = vec3(u_xlat22) * u_xlat3.xyz + vec3(u_xlat23);
    u_xlat3.xyz = (-u_xlat4.xyz) + u_xlat3.xyz;
    u_xlat3.xyz = vec3(vec3(_Blend, _Blend, _Blend)) * u_xlat3.xyz + u_xlat4.xyz;
    u_xlat4.xyz = vs_TEXCOORD5.yyy * hlslcc_mtx4x4unity_WorldToLight[1].xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToLight[0].xyz * vs_TEXCOORD5.xxx + u_xlat4.xyz;
    u_xlat4.xyz = hlslcc_mtx4x4unity_WorldToLight[2].xyz * vs_TEXCOORD5.zzz + u_xlat4.xyz;
    u_xlat4.xyz = u_xlat4.xyz + hlslcc_mtx4x4unity_WorldToLight[3].xyz;
    u_xlatb22 = unity_ProbeVolumeParams.x==1.0;
    if(u_xlatb22){
        u_xlatb22 = unity_ProbeVolumeParams.y==1.0;
        u_xlat6.xyz = vs_TEXCOORD5.yyy * hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[1].xyz;
        u_xlat6.xyz = hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[0].xyz * vs_TEXCOORD5.xxx + u_xlat6.xyz;
        u_xlat6.xyz = hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[2].xyz * vs_TEXCOORD5.zzz + u_xlat6.xyz;
        u_xlat6.xyz = u_xlat6.xyz + hlslcc_mtx4x4unity_ProbeVolumeWorldToObject[3].xyz;
        u_xlat6.xyz = (bool(u_xlatb22)) ? u_xlat6.xyz : vs_TEXCOORD5.xyz;
        u_xlat6.xyz = u_xlat6.xyz + (-unity_ProbeVolumeMin.xyz);
        u_xlat6.yzw = u_xlat6.xyz * unity_ProbeVolumeSizeInv.xyz;
        u_xlat22 = u_xlat6.y * 0.25 + 0.75;
        u_xlat23 = unity_ProbeVolumeParams.z * 0.5 + 0.75;
        u_xlat6.x = max(u_xlat22, u_xlat23);
        u_xlat6 = texture(unity_ProbeVolumeSH, u_xlat6.xzw);
    } else {
        u_xlat6.x = float(1.0);
        u_xlat6.y = float(1.0);
        u_xlat6.z = float(1.0);
        u_xlat6.w = float(1.0);
    }
    u_xlat22 = dot(u_xlat6, unity_OcclusionMaskSelector);
    u_xlat22 = clamp(u_xlat22, 0.0, 1.0);
    u_xlat23 = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat6 = texture(_LightTextureB0, vec2(u_xlat23));
    u_xlat4 = texture(_LightTexture0, u_xlat4.xyz);
    u_xlat23 = u_xlat4.w * u_xlat6.x;
    u_xlat22 = u_xlat22 * u_xlat23;
    u_xlat4.x = dot(vs_TEXCOORD2.xyz, u_xlat12.xyz);
    u_xlat4.y = dot(vs_TEXCOORD3.xyz, u_xlat12.xyz);
    u_xlat4.z = dot(vs_TEXCOORD4.xyz, u_xlat12.xyz);
    u_xlat23 = dot(u_xlat4.xyz, u_xlat4.xyz);
    u_xlat23 = inversesqrt(u_xlat23);
    u_xlat4.xyz = vec3(u_xlat23) * u_xlat4.xyz;
    u_xlat5.xyz = vec3(u_xlat22) * _LightColor0.xyz;
    u_xlat6.xyz = u_xlat3.xyz + vec3(-0.220916301, -0.220916301, -0.220916301);
    u_xlat6.xyz = vec3(vec3(_Metallic, _Metallic, _Metallic)) * u_xlat6.xyz + vec3(0.220916301, 0.220916301, 0.220916301);
    u_xlat22 = (-_Metallic) * 0.779083729 + 0.779083729;
    u_xlat3.xyz = vec3(u_xlat22) * u_xlat3.xyz;
    u_xlat22 = (-_Glossiness) + 1.0;
    u_xlat0.xyz = u_xlat0.xyz * vec3(u_xlat21) + u_xlat2.xyz;
    u_xlat21 = dot(u_xlat0.xyz, u_xlat0.xyz);
    u_xlat21 = max(u_xlat21, 0.00100000005);
    u_xlat21 = inversesqrt(u_xlat21);
    u_xlat0.xyz = vec3(u_xlat21) * u_xlat0.xyz;
    u_xlat21 = dot(u_xlat4.xyz, u_xlat2.xyz);
    u_xlat2.x = dot(u_xlat4.xyz, u_xlat1.xyz);
    u_xlat2.x = clamp(u_xlat2.x, 0.0, 1.0);
    u_xlat9 = dot(u_xlat4.xyz, u_xlat0.xyz);
    u_xlat9 = clamp(u_xlat9, 0.0, 1.0);
    u_xlat0.x = dot(u_xlat1.xyz, u_xlat0.xyz);
    u_xlat0.x = clamp(u_xlat0.x, 0.0, 1.0);
    u_xlat7.x = u_xlat0.x * u_xlat0.x;
    u_xlat7.x = dot(u_xlat7.xx, vec2(u_xlat22));
    u_xlat7.x = u_xlat7.x + -0.5;
    u_xlat14 = (-u_xlat2.x) + 1.0;
    u_xlat1.x = u_xlat14 * u_xlat14;
    u_xlat1.x = u_xlat1.x * u_xlat1.x;
    u_xlat14 = u_xlat14 * u_xlat1.x;
    u_xlat14 = u_xlat7.x * u_xlat14 + 1.0;
    u_xlat1.x = -abs(u_xlat21) + 1.0;
    u_xlat8 = u_xlat1.x * u_xlat1.x;
    u_xlat8 = u_xlat8 * u_xlat8;
    u_xlat1.x = u_xlat1.x * u_xlat8;
    u_xlat7.x = u_xlat7.x * u_xlat1.x + 1.0;
    u_xlat7.x = u_xlat7.x * u_xlat14;
    u_xlat14 = u_xlat22 * u_xlat22;
    u_xlat14 = max(u_xlat14, 0.00200000009);
    u_xlat1.x = (-u_xlat14) + 1.0;
    u_xlat8 = abs(u_xlat21) * u_xlat1.x + u_xlat14;
    u_xlat1.x = u_xlat2.x * u_xlat1.x + u_xlat14;
    u_xlat21 = abs(u_xlat21) * u_xlat1.x;
    u_xlat21 = u_xlat2.x * u_xlat8 + u_xlat21;
    u_xlat21 = u_xlat21 + 9.99999975e-06;
    u_xlat21 = 0.5 / u_xlat21;
    u_xlat14 = u_xlat14 * u_xlat14;
    u_xlat1.x = u_xlat9 * u_xlat14 + (-u_xlat9);
    u_xlat1.x = u_xlat1.x * u_xlat9 + 1.0;
    u_xlat14 = u_xlat14 * 0.318309873;
    u_xlat1.x = u_xlat1.x * u_xlat1.x + 1.00000001e-07;
    u_xlat14 = u_xlat14 / u_xlat1.x;
    u_xlat14 = u_xlat14 * u_xlat21;
    u_xlat14 = u_xlat14 * 3.14159274;
    u_xlat14 = max(u_xlat14, 9.99999975e-05);
    u_xlat7.y = sqrt(u_xlat14);
    u_xlat7.xy = u_xlat2.xx * u_xlat7.xy;
    u_xlat21 = dot(u_xlat6.xyz, u_xlat6.xyz);
    u_xlatb21 = u_xlat21!=0.0;
    u_xlat21 = u_xlatb21 ? 1.0 : float(0.0);
    u_xlat14 = u_xlat21 * u_xlat7.y;
    u_xlat1.xyz = u_xlat7.xxx * u_xlat5.xyz;
    u_xlat7.xyz = u_xlat5.xyz * vec3(u_xlat14);
    u_xlat0.x = (-u_xlat0.x) + 1.0;
    u_xlat22 = u_xlat0.x * u_xlat0.x;
    u_xlat22 = u_xlat22 * u_xlat22;
    u_xlat0.x = u_xlat0.x * u_xlat22;
    u_xlat2.xyz = (-u_xlat6.xyz) + vec3(1.0, 1.0, 1.0);
    u_xlat2.xyz = u_xlat2.xyz * u_xlat0.xxx + u_xlat6.xyz;
    u_xlat0.xyz = u_xlat7.xyz * u_xlat2.xyz;
    SV_Target0.xyz = u_xlat3.xyz * u_xlat1.xyz + u_xlat0.xyz;
    SV_Target0.w = 1.0;
    return;
}

#endif
                             $Globals$        _WorldSpaceCameraPos                         _WorldSpaceLightPos0                        unity_OcclusionMaskSelector                          unity_ProbeVolumeParams                   0      unity_ProbeVolumeSizeInv                  �      unity_ProbeVolumeMin                  �      _LightColor0                  �      _ColorRamp_ST                     �      _Glossiness                      	   _Metallic                          _Blend                      
   _BumpPower                         _Distortion                        _Hue                       _Saturation                        _Brightness                     	   _Contrast                           unity_ProbeVolumeWorldToObject                   @      unity_WorldToLight                   �          $GlobalsP  	      unity_WorldTransformParams                    �      _MainTex_ST                        _Mask_ST                     	   _Noise_ST                     0     _BumpMap_ST                   @     unity_ObjectToWorld                         unity_WorldToObject                  @      unity_MatrixVP                   �      unity_WorldToLight                   �             _MainTex                  _Noise                  _BumpMap                _Mask                
   _ColorRamp                  _LightTextureB0                 _LightTexture0                  unity_ProbeVolumeSH              