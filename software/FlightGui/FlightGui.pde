/** 
 * Top-level Processing sketch for running the FLIGHT GUI. To run
 * this, load it in Processing. Other sketches contain supporting code.
 *
 * @author Philip Levis <pal@cs.stanford.edu>
 */
import model.*;
import engine.*;
import heronarts.lx.*;
import heronarts.lx.audio.*;
import heronarts.lx.color.LXColor;
import heronarts.lx.effect.*;
import heronarts.lx.model.*;
import heronarts.lx.output.*;
import heronarts.lx.parameter.*;
import heronarts.lx.pattern.*;
import heronarts.lx.studio.LXStudio;
import heronarts.lx.transform.*;
import heronarts.lx.modulator.*;

import heronarts.p3lx.*;
import heronarts.p3lx.ui.*;
import heronarts.p3lx.ui.component.*;

import processing.opengl.*;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.PrintWriter;
import java.io.Reader;
import java.io.Writer;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Random;

import java.awt.Color;

final static int SECONDS = 1000;
final static int MINUTES = 60*SECONDS;
final static int BOTTOM_ROW_HEIGHT = 30;
double speedVal = 100;
float hueoffset = 0;
float satoffset = 100;
float brioffset = 100;
boolean masterOn = false;
Random rand = new Random();

static FlyerConfig[] flyerConfigurations;

FlightModel model;
ProcessingEngine pengine;
LXStudio lx;
UI3dContext viewContext;
FlightAPC40Listener apc40Listener;

// A FlyerHighlighter is an Effect that highlights a particular
// Fractal Flyer: flyerHighlighter is used to show which one you are
// moving around in the UI. -pal
FlyerHighlighter flyerHighlighter;


class ProcessingEngine extends FlightEngine {
    ProcessingEngine(String projectPath, FlightModel model, LXStudio lx) {
        super(projectPath, model, lx);
  }
}

// Derived from Mark Slee's LXStudioApp example
private static final String WINDOW_TITLE = "FLIGHT Graphical UI";
private static int WIDTH = 1280; 
private static int HEIGHT = 800;
private static boolean FULLSCREEN = false;

/**
 * Required by Processing. Sets up the graphics context/window size
 * and tells Processing to use a 3D optimized graphics engine.
 */

void settings() { size(WIDTH, HEIGHT, P3D); }

/**
 * Given an array of FlyerConfigs, builds the LXModel with flyers,
 * wings, and lights. Initializes the wings and bodies to red
 * and blue as defaults.
 */
void initializeModel(FlyerConfig[] flyers) {
  model = new Model(flyers).getFlightModel();
  List<LightSamplePointModel> wingLights = model.getAllWingsLights();
}

/**
 * Loads the Flyer configuration JSON file
 * for their positions and metadata, creates the data model from this
 * information, and creates an LXStudio instance. This method is required
 * and called by Processing.
 */

void setup() {
  frameRate(90);
  System.out.println("Starting LXStudio; initialized Fractal Flyer data model from " + Config.FLYER_CONFIG_FILE);
  IO io = new IO(sketchPath());
  flyerConfigurations = io.loadConfigFile(Config.FLYER_CONFIG_FILE);
  initializeModel(flyerConfigurations);
  lx = new LXStudio(this, model);
  lx.engine.setThreaded(true);
  //apc40Listener = new FlightAPC40Listener(lx);
}

/**
 * This method is required and called by Processing. Our implementation
 * does nothing.
 */
void initializeUI(LXStudio lx, LXStudio.UI ui) {}

/**
 * Creates the ProcessingEngine that triggers Patterns and generally
 * controls FLIGHT. Creates the FLIGHT GUI. This method is required
 * and called by Processing.
 */

public void onUIReady(LXStudio lx, LXStudio.UI ui) {
  System.out.println("Creating UI.");
  pengine = new ProcessingEngine(sketchPath(), model, lx);
      
  FlightGui.this.flyerHighlighter = new FlyerHighlighter(lx, flyerConfigurations, model);
  lx.addEffect(flyerHighlighter);
  createGUI(lx, ui);
}


/**
 * Creates the Processing-based GUI for FLIGHT.
 */
void createGUI(LXStudio lx, LXStudio.UI ui) {
    // Don't want all of the standard LXStudio stuff.
    ui.bottomTray.setVisible(false);
    ui.contentPicker.setVisible(false);
    ui.helpBar.setVisible(false);
    ui.leftPane.setVisible(false);
    ui.preview.setVisible(false);
    ui.rightPane.setVisible(false);

    // Set up the camera view the UI uses to display the FLIGHT
    // installation.
    // On VMware can't make a 3D context smaller than the whole
    // window, so comment out sizes for now.
    viewContext = new UI3dContext(ui, 0, 0, WIDTH, HEIGHT);
    /*{ //WIDTH/4, 0, WIDTH/2, HEIGHT-200) {
            protected void beforeDraw(LXStudio.UI ui, PGraphics pg) {}
            protected void afterDraw(LXStudio.UI ui, PGraphics pg) {
                pg.noLights();
            }
            };*/

    // Configure the camera to rotate around a center point that's
    // in the middle of the front window and 15 feet inside.
    // Also set some camera angle constants so things look OK. Something
    // more principled would be great. -pal
    viewContext.setTheta((float)(30.0 * Math.PI / 180.0));
    viewContext.setPhi((float) (10.0 * Math.PI / 180.0));
    viewContext.setCameraVelocity(10000.0);
    viewContext.setCameraAcceleration(10000.0);
    viewContext.setCenter(Geometry.FRONT_WINDOW_WIDTH / 2,
                          Geometry.FRONT_WINDOW_CORNER_HEIGHT / 4f,
                          20 * Geometry.FEET);
    viewContext.setInteractionMode(UI3dContext.InteractionMode.ZOOM);
    viewContext.setRadius(95 * Geometry.FEET);

    viewContext.addComponent(new UIFlightDrawer(model));
    // Adds the control panel for selecting and moving Flyers
    ui.addLayer(viewContext);
    ui.addLayer(new ScriptRateWindow(ui));
    MasterControl masterControl = new MasterControl(ui);
    ui.addLayer(masterControl);
    ui.addLayer(new FlyerLayoutControlWindow(ui));
    ChannelMixerWindow mixer = new ChannelMixerWindow(lx, ui);
    ui.addLayer(mixer);
    PatternControlWindow patternWindow = new PatternControlWindow(lx, ui);
    ui.addLayer(patternWindow);
    apc40Listener = new FlightAPC40Listener(lx, patternWindow.patternLists, masterControl.HueKnob, masterControl.SatKnob, masterControl.BriKnob, masterControl.SpeedKnob);
    ui.addLayer(new ClipPlayingWindow(lx, ui, pengine.playlist));
    //ui.addLayer(new FlightPatternConfig(lx, ui));
}

@Override
public void draw() {
  List<FlyerModel> flyers = model.getFlyers();
  LXChannel channel = lx.engine.mixer.getDefaultChannel();
  lx.setSpeed(speedVal/100);
  // LXStudio takes care of mixing the colors from all of the different
  // active channels playing patterns on the Fractal Flyers. The wing
  // position, however, is handled differently. It doesn't make sense to
  // mix it like you mix color. So each FlightPattern object stores
  // a desired wing position that the LXStudio engine isn't aware of.
  // The GUI copies the wing position out of the pattern in Channel 1
  // here: this means channel 1 controls wing position. -pal
  try {
      FlightPattern fp = (FlightPattern)channel.getActivePattern();
      for (FlyerModel flyer: flyers) {
          WingModel left = (WingModel)flyer.getLeftWing();
          WingModel right = (WingModel)flyer.getRightWing();
          left.setSkew(fp.getSkew(left));
          right.setSkew(fp.getSkew(right));
      }
  } catch (ClassCastException ex) {
      System.err.println("There is a non-FlightPattern in the pattern list.");
      System.err.println(channel.getFocusedPattern());
      ex.printStackTrace();
  }
  if(masterOn){
	  for (int i = 0; i < flyers.size(); i++) {
		  List<LightSamplePointModel> points = model.getFlyerLights(i);
		  LXEngine.Frame frame = lx.getUIFrame();
		  int colors[] = frame.getColors();
		  for(LightSamplePoint lsp: points){
			  String hex = Integer.toHexString(colors[lsp.getIndex()]);
			  if(hex.length() < 8){
			  }
			  else{
				  int r = Integer.parseInt(hex.substring(2,4), 16);
				  int g = Integer.parseInt(hex.substring(4,6), 16);
				  int b = Integer.parseInt(hex.substring(6,8), 16);
				  float[] hsbvals = Color.RGBtoHSB(r,g,b,null);
				  frame.getColors()[lsp.getIndex()] = LX.hsb((hsbvals[0]*360 + hueoffset)%360, (hsbvals[1] * satoffset), (hsbvals[2] * brioffset));
			  }
		  }
	  }
  }
}

void keyPressed() {
	switch (key) {
		case 'a':
			System.out.println("Pressed a");
			break;
	}
}
