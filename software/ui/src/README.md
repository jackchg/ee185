# FLIGHT UI

This directory stores the source files and classes that interface with LX Studio in order to run the FLIGHT UI.

## File descriptions
  - Config: Configuration settings used to initiate the UI engine 
  - Engine: Sets up the LX interface including the model, generator outputs, effects and channels
  - Flight: Interface that describes a flight installation and accompanied functions 
  - Flyer: Interface that describes a flyer and accompanied functions
  - Generator: Translates the state of the flyer on every loop of the engine into python commands
  - Geometry: Interface that describes the geometry of the flyer body, wings and light points
  - IO: Contains functions for reading and writing from files
  - LightSamplePoint: Describes the geometry of a single light point on the flyer
  - Model: Defines the flight model, flyer model and wing model as extensions of LXModel to interface with LXStudio
  - RunHeadless: Runs the FLIGHT UI without having a graphical UI, this contains a **main()** function
  - Wing: Interface that describes a flyer wing and accompanied functions
  
## Operation
Before starting, make sure the configuration settings in Config.java are the correct options.
 
To start the UI engine, any top-level class creates an **Engine** with the top-level project path. The class has to then 
describe the type of LX object, either P3LX or normal LX by defining the **createLX()** function in the Engine.

The engine will then create a FlightModel from the flyer configurations saved and configure the generator outputs.

## Tracks and Playlists Abstractions
Each of the 8 channels have multiple clips that can be used, but only the first one (index 0) is used. In the "Clip playing window" -- should be changed to "playlist window" after all commits -- you can record a new track which records all the parameter changes that happen on clip 0 across all channels to create a TRACK. TRACKs can be composed into playlists by queuing them together. Each track has a "multiplicity" next to it on the UI which just adds multiple copies of the track to the Queue. The flight engine maintains a playQueue, trackLoops, trackLoopsCounter, and trackNames lists to update this. Saving, saves the full playlist as a JSON array of multiple tracks. Implementation of track is in Track.java and FlightTrack.java.
