package model;
import java.util.List;

/*
 * The naming scheme for the pattern getter functions are as follows:
 * get [required]
 * all flyers or one flyer (All vs Flyer) [required]
 * part specifier if applicable (LeftWing(s), RightWing(s), Wings, Body) [optional]
 * light specifier if applicable (PositionIndexed) [optional]
 * Light/Lights
 */
public interface Flight extends Geometry {
  /**
   * pattern getters for all flyers
   */
  /* ------------- all flyers, motion ------------- */
  List<? extends Wing> getAllWings();
  List<? extends Wing> getAllLeftWings();
  List<? extends Wing> getAllRightWings();
  int[] getAllWingsSkew();
  int[] getAllLeftWingsSkew();
  int[] getAllRightWingsSkew();

  /* ------------- all flyers, lights ------------- */
  List<? extends LightSamplePoint> getAllLights();

  List<? extends LightSamplePoint> getAllBodyLights();
  List<? extends LightSamplePoint> getAllWingsLights();
  List<? extends LightSamplePoint> getAllWingsPositionIndexedLights(int positionIndex); // head

  // left wings
  List<? extends LightSamplePoint> getAllLeftWingLights();
  List<? extends LightSamplePoint> getAllLeftWingPositionIndexedLights(int positionIndex);

  // right wings
  List<? extends LightSamplePoint> getAllRightWingLights();
  List<? extends LightSamplePoint> getAllRightWingPositionIndexedLights(int positionIndex);


  /**
   * pattern getters for individual flyers
   */
  /* ------------- one flyer, motion ------------- */
  List<? extends Wing> getFlyerWings(int flyerIndex);
  Wing getFlyerLeftWing(int flyerIndex);
  Wing getFlyerRightWing(int flyerIndex);
  int[] getFlyerWingsSkew(int flyerIndex);
  int getFlyerLeftWingSkew(int flyerIndex);
  int getFlyerRightWingSkew(int flyerIndex);

  /* ------------- one flyer, lights ------------- */
  List<? extends LightSamplePoint> getFlyerLights(int flyerIndex);

  LightSamplePoint getFlyerBodyLight(int flyerIndex);
  List<? extends LightSamplePoint> getFlyerWingsLights(int flyerIndex);
  List<? extends LightSamplePoint> getFlyerWingsPositionIndexedLights(int flyerIndex, int positionIndex);

  List<? extends LightSamplePoint> getFlyerLeftWingLights(int flyerIndex);
  LightSamplePoint getFlyerLeftWingPositionIndexedLights(int flyerIndex, int positionIndex);

  List<? extends LightSamplePoint> getFlyerRightWingLights(int flyerIndex);
  LightSamplePoint getFlyerRightWingPositionIndexedLights(int flyerIndex, int positionIndex);


  /**
   * flyer config getters
   */
  Flyer getFlyer(int i);

  /**
   * This defines the positions of the flyers, which are
   * index,
   * x (left to right), y (front to back), z (bottom to top),
   * rotation in degrees, and tilt (3 options)
   */
  FlyerConfig[] getFlyerConfigs();

  List<? extends Flyer> getFlyers();
}